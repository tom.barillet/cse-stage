<?php
/**
 * Contrôleur de la page d'accueil
 */

namespace controleur;

use vue\accueil\VueAccueil;
use vue\accueil\VueMentions;
use vue\accueil\VueActionNonAutorisee;

class CtrlAccueil extends ControleurGenerique {

    /** controleur= accueil & action= defaut
     * Afficher la page d'accueil      */
    function defaut() {
        $this->vue = new VueAccueil();
        $this->vue->setTitre("CSE La Joliverie - Accueil");
        if (SessionAuthentifiee::estConnecte()) {
            parent::controlerVueAutorisee();
        }else{
            parent::controlerVueNonAutorisee();
        }
        $this->vue->afficher();
    }
    
    function mentions(){
        $this->vue = new VueMentions();
        $this->vue->setTitre("CSE La Joliverie - Mentions Légales");
        parent::controlerVueNonAutorisee();
        $this->vue->afficher();

    }
    /** controleur= accueil & action= refuser
    * En cas de tentative d'accès à une fonctionnalité nécessitant authentification  */
    function refuser() {
        $this->vue = new VueActionNonAutorisee();
        $this->vue->setTitre("CSE La Joliverie - Accueil");
        parent::controlerVueNonAutorisee();
        $this->vue->afficher();
    }

}
