<?php
/**
 * Contrôleur fonction d'authentification
 * @author Barillet/Menadier
 * @version 2021
 */

namespace controleur;

use controleur\GestionErreurs;

use modele\dao\SalarieDAO;
use modele\dao\ContactDAO;
use modele\metier\Salarie;
use modele\dao\Bdd;
use modele\dao\SiteDAO;
use modele\metier\Site;
use modele\metier\Email;
use modele\dao\EmailDAO;

use vue\accueil\VueAccueil;
use vue\authentification\VueSAuthentifier;
use vue\inscription\VueInscriptionSalarie;
use vue\contact\VueContact;

class CtrlAuthentification extends ControleurGenerique {

    /** controleur= authentification & action= defaut  */
    function defaut() {
        $this->saisirIdentification();
    }
    

    /** controleur= authentification & action=saisirIdentification
     * Afficher le formulaire d'identification d'un utilisateur     */
    public function saisirIdentification() {
        $laVue = new VueSAuthentifier();
        $this->vue = $laVue;
        $this->vue->setTitre("CSE La Joliverie - authentification");

        $laVue->setMessage("Identification");
        $laVue->setAdressemail("");
        $laVue->setMotdepasse("");
        
        if (SessionAuthentifiee::estConnecte()) {
            parent::controlerVueAutorisee();
        }else{
            parent::controlerVueNonAutorisee();
        }
        
        $this->vue->afficher();
    }

    /** controleur= authentification & action= authentifier
     * vérifier l'identité de l'utilisateur dans la BDD    */
    public function authentifier() {
        Bdd::connecter();
        $mail = $_REQUEST['adressemail'];
        $mdp = $_REQUEST['motdepasse'];
        $utilisateurConnecte = $this->verifierDonneesIdentification($mail, $mdp);
        if (GestionErreurs::nbErreurs() == 0) {
            // L'utilisateur est authentifiée
            // Initialiser la session
            SessionAuthentifiee::seConnecter($utilisateurConnecte);
            // Afficher l'écran d'accueil
            header("Location: index.php");
        } else {
            // Erreur d'identification
            // Revenir à la demande de connexion
            $laVue = new VueSAuthentifier();
            $this->vue = $laVue;
            $this->vue->setTitre("CSE La Joliverie - authentification");
 
            $laVue->setMessage("Identification");
            $laVue->setAdressemail($mail);
            $laVue->setMotdepasse($mdp);
            parent::controlerVueNonAutorisee();
            $this->vue->afficher();
        }
    }
    

    /** controleur= authentification & action=inscription
     * Afficher le formulaire d'inscsription d'un salarié     */
    public function inscription() {
        $laVue = new VueInscriptionSalarie();
        $this->vue = $laVue;
        $laVue->setActionRecue("inscription");
        $laVue->setActionAEnvoyer("validerInscription");
        $laVue->setMessage("Vous êtes inscrit");
        // En création, on affiche un formulaire vide
        $idSite = new Site("", "");
        /* @var Salarie $unSalarie */
        $unSalarie = new Salarie("", $idSite, "", "", "", "", "", "");
        $laVue->setUnSalarie($unSalarie);
        parent::controlerVueNonAutorisee();

        Bdd::connecter();
        $lesSites = SiteDAO::getAll();
        $this->vue->setLesSites($lesSites);

        $this->vue->setTitre("CSE La Joliverie - inscription");
        $this->vue->afficher();
    }
   
    /** controleur= authentification & action=validerInscription
     * ajout d'un salarié dans la base de données d'après la saisie    */
    public function validerInscription() {
        function better_crypt($input, $rounds = 7){
        $salt = "";
        $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
        for($i=0; $i < 22; $i++) {
            $salt .= $salt_chars[array_rand($salt_chars)];
        }
        return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt);
        }
        Bdd::connecter();
        /* @var Salarie $unSalarie  : récupération du contenu du formulaire et instanciation d'un salarié */
        $unSalarie = new Salarie(null, SiteDAO::getOneById($_REQUEST['site']), $_REQUEST['adressemail'], $_REQUEST['nom'], $_REQUEST['prenom'], 
        $_REQUEST['typeSalarie'],  better_crypt($_REQUEST['motdepasse'],10), 0);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        //$utilisateurInscrit = $this->verifierDonneesInscription($mailInscrit);
        $unMail = $unSalarie->getEmail();
        if(!EmailDAO::isAnExistingMail($unMail)){
            GestionErreurs::ajouter("L'email saisi est incorrect.");
        }
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer le salarié
            SalarieDAO::insert($unSalarie);
            // revenir à la page de connexion
            header("Location: index.php?controleur=authentification&action=saisirIdentification");
            
        } else {
            // s'il y a des erreurs,
            // réafficher le formulaire d'inscription
            $laVue = new VueInscriptionSalarie();
            $this->vue = $laVue;
            $laVue->setActionRecue("Inscription");
            $laVue->setActionAEnvoyer("validerInscription");
            $laVue->setMessage("Vous êtes inscrit");
            $lesSites = SiteDAO::getAll();
            $this->vue->setLesSites($lesSites);
            $laVue->setUnSalarie($unSalarie);
            parent::controlerVueNonAutorisee();
            $laVue->setTitre("CSE La Joliverie - inscription");
            $this->vue->afficher();
        }
    }
    
    /** controleur= contact & action= defaut
     * Afficher la page de contact      */
    function defautNonConnecte() {

        $this->vue = new VueContact();
         $this->vue->setTitre("CSE La Joliverie - Contact");
         Bdd::connecter();
        $lesContacts = ContactDAO::getAll();
        $lesSalaries = array();
        foreach ($lesContacts as $unContact){
            $lesSalaries[] = SalarieDAO::getOneByMail($unContact->getMailContact());
        }
        $this->vue->setLesSalaries($lesSalaries);
        $this->vue->setTitre("CSE La Joliverie - Contact");
        parent::controlerVueNonAutorisee();
        $this->vue->afficher();
    }
    
    /**
     * controleur= authentification & action= seDeconnecter
     * = mettre fin à la session   */
    public function seDeconnecter() {
        // Mettre fin à la session
        SessionAuthentifiee::seDeconnecter();
        // Afficher l'écran d'accueil
        header("Location: index.php");
    }
    
    /*********************************************************************************************************
     *  Méthodes privées
     *********************************************************************************************************/

    /**
     * Vérification des données saisies dans le formulaire d'identification
     * @param string $mailSaisi
     * @param string $mdpSaisi
     * @return Utilisateur : l'utilisateur concerné si le loginet le mot de passe correspondent; =null sinon
     * EFFET DE BORD : génération de messages d'erreur dans la variable de session
     */
    private function verifierDonneesIdentification($mailSaisi, $mdpSaisi) {
        $unUtilisateur = null;
        if ($mailSaisi == "") {
            GestionErreurs::ajouter('Il faut saisir votre e-mail');
        } else {
            /* @var $unUtilisateur Utilisateur  */
            $unUtilisateur = SalarieDAO::getOneByMail($mailSaisi);
            if (is_null($unUtilisateur)) {
                // Aucun utilisateur inscrit n'a ce login
                GestionErreurs::ajouter('Mot de passe ou e-mail incorrect.');
            } else {
                // L'utilisateur (login) existe
                $mdpValide = $unUtilisateur->getMdp();
                if (!hash_equals($mdpValide, crypt($mdpSaisi, $mdpValide))) {
                    // Le mot de passe enregistré ne correspond pas au mot de passe fourni
                    GestionErreurs::ajouter('Mot de passe ou e-mail incorrect.');
                }
            }
        }
        return $unUtilisateur;
    }
    
//    /**
//     * Vérification des données saisies dans le formulaire d'inscription
//     * @param string $mailInscrit
//     * @return Utilisateur : l'utilisateur concerné si le login correspond; =null sinon
//     * EFFET DE BORD : génération de messages d'erreur dans la variable de session
//     */
//    private function verifierDonneesInscription($mailInscrit) {
//        $unUtilisateur = null;
//        if ($mailInscrit === "") {
//            GestionErreurs::ajouter('Il faut saisir votre e-mail');
//        } else {
//            /* @var $unUtilisateur Utilisateur  */
//            $unUtilisateur = EmailDAO::getOneByMail($mailInscrit);
//            if (is_null($unUtilisateur)) {
//                // Aucun utilisateur inscrit n'a ce login
//                GestionErreurs::ajouter('login incorrect 1');
//            } else {
//                // L'utilisateur (login) existe
//                $mdpValide = $unUtilisateur->getMdp();
//            }
//        }
//        return $unUtilisateur;
//    }
//
}
