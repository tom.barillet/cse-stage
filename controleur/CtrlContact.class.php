<?php
/**
 * Contrôleur de la page de contact
 */

namespace controleur;

use vue\contact\VueContact;
use modele\dao\ContactDAO;
use modele\dao\SalarieDAO;
use modele\dao\Bdd;



class CtrlContact extends ControleurGenerique {

    /** controleur= contact & action= defaut
     * Afficher la page de contact      */
    function defaut() {
       
        $this->vue = new VueContact();
         $this->vue->setTitre("CSE La Joliverie - Contact");
         Bdd::connecter();
        $lesContacts = ContactDAO::getAll();
        $lesSalaries = array();
        foreach ($lesContacts as $unContact){
            $lesSalaries[] = SalarieDAO::getOneByMail($unContact->getMailContact());
        }
        $this->vue->setLesSalaries($lesSalaries);
        $this->vue->setTitre("CSE La Joliverie - Contact");
        parent::controlerVueAutorisee();
        $this->vue->afficher();

    }

   
}
