<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\SalarieDAO;
use modele\metier\Salarie;
use modele\metier\Site;
use modele\dao\SiteDAO;
use modele\dao\EnfantDAO;
use modele\dao\Bdd;
use controleur\SessionAuthentifiee;
use vue\enfant\VueSaisieEnfant;
use vue\enfant\VueSaisieNombreEnfant;
use vue\enfant\VueListeSalarieEnfants;
use vue\enfant\VueSupprimerEnfant;
use vue\enfant\VueModifEnfant;
use modele\metier\Enfant;
use modele\dao\FamilleDAO;
use modele\metier\Famille;

class CtrlEnfant extends ControleurGenerique {

    /** controleur= infos & action= defaut
     * Afficher la liste des infos      */
    public function defaut() {
        $this->creer();
    }
    
    /** controleur= enfant & action=creer
     * Afficher le formulaire d'ajout d'un enfant     */
    public function creer() {
        $laVue = new VueSaisieEnfant();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Ajouter un enfant");
        // En création, on affiche un formulaire vide
        /* @var Enfant $unEnfant */
        $unEnfant = new Enfant("", "", "", "");
        $laVue->setUnEnfant($unEnfant);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("CSE La Joliverie - Enfant");
        $this->vue->afficher();
    }
    
    /** controleur= enfant & action=validerCreer
     * ajouter d'un enfant dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Enfant $unEnfant  : récupération du contenu du formulaire et instanciation d'un Enfant */
        $unEnfant = new Enfant(null,$_REQUEST['nom'],  $_REQUEST['prenom'], $_REQUEST['dateNaissance']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer l'enfant
            EnfantDAO::insert($unEnfant);
            $Enfant = EnfantDAO::getIdByEnfant($unEnfant->getNom(),$unEnfant->getPrenom(), $unEnfant->getDateNaissance());
            //var_dump($Enfant);
            $Salarie = SessionAuthentifiee::getUtilisateur();
            $uneFamille = new Famille($Salarie, $Enfant);
            FamilleDAO::insert($uneFamille);
            // revenir à la liste des informations
            header("Location: index.php?controleur=infos&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieEnfant();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Enfant ajouté");
            $laVue->setUnEnfant($unEnfant);
            parent::controlerVueAutorisee();
            $laVue->setTitre("CSE La Joliverie - Enfant");
            $this->vue->afficher();
        }
    }


    
}
