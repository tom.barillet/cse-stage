<?php

/**
 * Controleur de la page d'information d'un salarié
 *
 * @author Barillet/Menadier
 * @version 2021
 */


namespace controleur;

use controleur\GestionErreurs;
use modele\dao\SalarieDAO;
use modele\metier\Salarie;
use modele\metier\Cadeau;
use modele\metier\Site;
use modele\dao\SiteDAO;
use modele\dao\CadeauDAO;
use modele\dao\EnfantDAO;
use modele\dao\Bdd;
use controleur\SessionAuthentifiee;
use vue\informations\VueListeInfos;
use vue\informations\VueDetailEnfantInfos;
use vue\informations\VueDetailInfos;

class CtrlInfos extends ControleurGenerique {

    /** controleur= infos & action= defaut
     * Afficher la liste des infos      */
    public function defaut() {
        $this->liste();
    }
     /** controleur= infos & action=liste & id=identifiant_salarie
     * Afficher la liste des info d'un salarié d'après son identifiant récupéré via la session    */
    public function liste() {
        $laVue = new VueListeInfos();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des informations
        Bdd::connecter();
        $unSalarie = SessionAuthentifiee::getUtilisateur();
        $idSalarie = $unSalarie->getId();
        $laVue->setUnSalarie($unSalarie);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("CSE La Joliverie - Informations");
        $this->vue->afficher();
    }
    
    /** controleur= infos & action=detail & id=identifiant_salarie
     * Afficher un salarié d'après son identifiant     */
    public function detail() {
        $idSalarie = $_GET["id"];
        $this->vue = new VueDetailInfos();
        Bdd::connecter();
        $this->vue->setUnSalarie(SalarieDAO::getOneById($idSalarie));
        $this->vue->setEnfants(EnfantDAO::getAllById($idSalarie));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
    
    //Affichage des détails des enfants (nom, prénom, date de naissance et cadeau choisi)
    public function detailEnfant(){  
        $idEnfant = $_GET["idEnfant"];
        $this->vue = new VueDetailEnfantInfos();
        Bdd::connecter();
        $this->vue->setUnEnfant(EnfantDAO::getOneById($idEnfant));
        $unCadeau = CadeauDAO::getOneByIdEnfant($idEnfant);
        if(is_null($unCadeau)){
            $unCadeau = new Cadeau(99, "Cadeau non choisi");
        }
        $this->vue->setUnCadeau($unCadeau);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();

        
    }

     public function getTabInfosAvecNbAttributions(): Array {
        $lesInfosAvecNbAttrib = Array();
        $lesInfos = SessionAuthentifiee::getUtilisateur();
        foreach ($lesInfos as $uneInfo) {
            /* @var Salarie $uneInfo */
            $lesInfosAvecNbAttrib[$uneInfo->getId()]['informations'] = $uneInfo;
        }
        return $lesInfosAvecNbAttrib;
    }
}
