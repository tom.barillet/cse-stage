<?php
/**
 * Contrôleur de la page de contact
 */

namespace controleur;

use vue\noel\VueNoelListe;
use vue\noel\VueNoelChoix;
use vue\noel\VueNoelConfirmation;
use modele\dao\AnneeEnfantDAO;
use modele\dao\AnneeParentDAO;
use modele\dao\EnfantDAO;
use modele\metier\AnneeEnfant;
use modele\metier\AnneeParent;
use modele\dao\Bdd;

class CtrlNoel extends ControleurGenerique {

    /** controleur= noel & action= defaut
     * Afficher la page de contact      */
    function defaut() {
       $this->liste();
    }
    
    //Liste des enfants de moins de 13 ans pour le choix des cadeaux.
    public function liste() {
        $laVue = new VueNoelListe();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des informations
        Bdd::connecter();
        $unSalarie = SessionAuthentifiee::getUtilisateur();
        $idSalarie = $unSalarie->getId();
        $this->vue->setUnSalarie($unSalarie);
        $lesEnfants = EnfantDAO::getAllById($idSalarie);
        foreach ($lesEnfants as $unEnfant){
            $idEnfant = $unEnfant->getId();
            if(EnfantDAO::getOneAgeById($idEnfant)<13){
                $listeEnfant[] = $unEnfant;
                $this->vue->setEnfants($listeEnfant);
            }
            else{
               $listeEnfant;
            }
        }
        parent::controlerVueAutorisee();
        $this->vue->setTitre("CSE La Joliverie - Noël");
        $this->vue->afficher();
    }
    //Affichage du choix des cadeaux pour chaque enfant - en fonctino de l'id enfant
    public function choixEnfant(){
        $idEnfant = $_GET["idEnfant"];
        $this->vue= new VueNoelChoix();
        Bdd::connecter();
        $unEnfant = EnfantDAO::getOneById($idEnfant); 
        $this->vue->setUnEnfant($unEnfant);
        parent::controlerVueAutorisee();
        $this->vue->setActionRecue("choix");
        $this->vue->setActionAEnvoyer("validerChoix");
        $this->vue->setTitre("CSE La Joliverie - Noël");
        $this->vue->afficher();
    } 
    
    //Valider le choix des parents(panier garni)
    public function validerChoixParent(){
        $unSalarie = SessionAuthentifiee::getUtilisateur();
        $idSalarie = $unSalarie->getId();
       $uneAnneeParent = new AnneeParent($idSalarie,$_REQUEST['panier'],null);
        Bdd::connecter();
        
        if (AnneeParentDAO::isAnExistingId($uneAnneeParent->getIdSalarie())){
            GestionErreurs::ajouter("Vous avez déjà choisi le panier garni");
            header("Location: index.php?controleur=noel&action=liste");
        }
        else {
           AnneeParentDAO::insert($uneAnneeParent);
           header("Location: index.php?controleur=noel&action=confirmation");
        }
    }
    //Confirmation du choix du cadeau du salarié
    public function confirmation(){
        $this->vue= new VueNoelConfirmation();
                parent::controlerVueAutorisee();

        $this->vue->setTitre("CSE La Joliverie - Noël");
        $this->vue->afficher();
    }
        
    //Valider le choix du cadeau des enfants(jouet ou chèque)
    public function validerChoix(){
        
        $uneAnneeEnfant = new AnneeEnfant($_REQUEST['idEnfant'],$_REQUEST['idCadeau'],null);
        Bdd::connecter();
        
        if (AnneeEnfantDAO::isAnExistingId($uneAnneeEnfant->getIdEnfant())){
            GestionErreurs::ajouter("Vous avez déjà choisi un cadeau pour cet enfant");
            header("Location: index.php?controleur=noel&action=liste");
        }
        else {
           AnneeEnfantDAO::insert($uneAnneeEnfant);
           header("Location: index.php?controleur=noel&action=liste");
        }
    }
     
}

