<?php
namespace controleur;

use modele\dao\SalarieDAO;
use modele\dao\SiteDAO;
use modele\dao\EnfantDAO;
use modele\dao\ContactDAO;
use modele\dao\FamilleDAO;
use modele\dao\SalarieEnfantDAO;


use modele\metier\Salarie;
use modele\metier\Enfant;
use modele\metier\SalarieEnfant;
use modele\metier\Contact;

use modele\dao\Bdd;

use vue\accueil\VueMentions;

use vue\salaries\VueListeSalarie;
use vue\salaries\VueDetailSalarie;
use vue\salaries\VueSupprimerSalarie;
use vue\salaries\VueSaisieSalarie;

use vue\enfant\VueListeSalarieEnfants;
use vue\enfant\VueSupprimerEnfant;
use vue\enfant\VueModifEnfant;

use vue\pdf\VueGenererPanierPDF;
use vue\pdf\VueGenererJouetPDF;
use vue\pdf\VueGenererChequePDF;

use vue\csv\VueGenererJouetCSV;


class CtrlSalaries extends ControleurGenerique {

    /** controleur= salarie & action= defaut
     * par défaut, afficher la liste des groupes      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= salaries & action= liste
     * Afficher la liste des groupe de chambres       */
    function liste() {
        $laVue = new VueListeSalarie();
        $this->vue = $laVue;
        // On récupère un tableau composé d'objets de type Salarié lus dans la BDD
        Bdd::connecter();
        $laVue->setSalarie(SalarieDAO::getAll());
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
/** controleur= salaries & action=detail & id=identifiant_salarie
     * Afficher un salarié d'après son identifiant     */
    public function detail() {
        $idSalarie = $_GET["id"];
        $this->vue = new VueDetailSalarie();
        // Lire dans la BDD les données du Groupe à afficher
        Bdd::connecter();
        $this->vue->setUnSalarie(SalarieDAO::getOneById($idSalarie));
        $this->vue->setEnfants(EnfantDAO::getAllById($idSalarie));
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
        /** controleur= salarie & action=supprimer & id=identifiant_salarie
     * Supprimer un salarié d'après son identifiant     */
    public function supprimer() {
        $idSalarie = $_GET["id"];
        $laVue = new VueSupprimerSalarie();
        $this->vue = $laVue;
        // Lire dans la BDD le salarié à supprimer
        Bdd::connecter();
        $laVue->setUnSalarie(SalarieDAO::getOneById($idSalarie));
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
   
    
        /** controleur= salarié & action= validerSupprimer & id = id_salarié
     * supprimer un salarié dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du salarié à supprimer");
        } else {
            
            if(FamilleDAO::isAnExistingId($_GET["id"])){
                SalarieDAO::deleteWithChild($_GET["id"]);

            } else {
                SalarieDAO::deleteWithoutChild($_GET["id"]);
            }         
        // suppression du salarié d'après son identifiant
        }
        header("Location: index.php?controleur=salaries&action=liste");
    }
  
    
         /** controleur= salarie & action=modifier & id = id salarie
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idSalarie = $_GET["id"];
        $laVue = new VueSaisieSalarie();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        $unSalarie = SalarieDAO::getOneById($idSalarie);
        $laVue->setUnSalarie($unSalarie);
        $lesSites = SiteDAO::getAll();
        if(ContactDAO::isAnExistingMail($laVue->getUnSalarie()->getEmail())){
            $laVue->setContact(true);
        }
        else {
            $laVue->setContact(false);  
        }
        $laVue->setLesSites($lesSites);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le salarié : " . $laVue->getUnSalarie()->getId());
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
    
             /** controleur= salarie & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un Groupe */
         $unSalarie = new Salarie($_REQUEST['id'], SiteDAO::getOneById($_REQUEST['site']), $_REQUEST['adressemail'], $_REQUEST['nom'], $_REQUEST['prenom'], 
        $_REQUEST['typeSalarie'], $_REQUEST['motdepasse'],0);
         //echo $unSalarie;
      
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesSalarie($unSalarie, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications
            if ($_REQUEST['contact'] == 1 ){
                $unContact = new Contact(null,$unSalarie->getEmail());
                SalarieDAO::update($unSalarie->getId(), $unSalarie);
                ContactDAO::insert($unContact);
            }
            else {
                ContactDAO::deleteWithEmail($unSalarie->getEmail());
                SalarieDAO::update($unSalarie->getId(), $unSalarie);
            }
           
            // revenir à la liste des Groupes
           header("Location: index.php?controleur=salaries&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieSalarie();
            $this->vue = $laVue;
            $laVue->setUnSalarie($unSalarie);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le salarié : " . $laVue->getUnSalarie()->getNom()
                    . " (" . $laVue->getUnSalarie()->getNom() . ")");
            $this->vue->setTitre("CSE La Joliverie - Salariés");
            parent::controlerVueAdmin();
            $this->vue->afficher();
        }
    }
      //Permet à un administrateur de se déconnecter
        public function seDeconnecter() {
        // Mettre fin à la session
        SessionAuthentifiee::seDeconnecter();
        // Afficher l'écran d'accueil
        header("Location: index.php");
    }
    
    private function verifierDonneesSalarie(Salarie $unSalarie, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if ($creation && $unSalarie->getPrenom() == "" || $unSalarie->getNom() == ""|| $unSalarie->getEmail() == ""|| 
                $unSalarie->getMdp() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unSalarie->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
                if (SalarieDAO::isAnExistingId($unSalarie->getId())) {
                    GestionErreurs::ajouter("Le salarié " . $unSalarie->getId() . " existe déjà");
                }
        }
        if ($unSalarie->getNom() != "" && SalarieDAO::isAnExistingNom(true, $unSalarie->getId(), $unSalarie->getNom())) {
            GestionErreurs::ajouter("Le salarié " . $unSalarie->getNom() . " existe déjà");
        }
    }
     /** controleur= salarié & action=supprimerEnfant & id=identifiant_enfant
     * Supprimer un enfant d'après son identifiant     */
    public function supprimerEnfant() {
        $idEnfant = $_GET["id"];
        $laVue = new VueSupprimerEnfant();
        $this->vue = $laVue;
        // Lire dans la BDD le salarié à supprimer
        Bdd::connecter();
        $laVue->setUnEnfant(EnfantDAO::getOneById($idEnfant));
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
    
        /** controleur= salarié & action= validerSupprimer & id = id_salarié
     * supprimer un enfant dans la base de données après confirmation   */
    public function validerSupprimerEnfant() {
        Bdd::connecter();
        $idEnfant = $_GET["id"];
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de l'enfant à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            EnfantDAO::delete($idEnfant); 
        }
        // retour à la liste des Groupe
        header("Location: index.php?controleur=salaries&action=liste");
    }
    
    //Affichage de tous les enfants d'un salarié pour choisir lequel modifier 
    public function modifierEnfantsListe() {
        $laVue = new VueListeSalarieEnfants();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des informations
        Bdd::connecter();
        $idSalarie = $_GET["id"];
        $unSalarie = SalarieDAO::getOneById($idSalarie);
        $this->vue->setUnSalarie($unSalarie);
        $lesEnfants = EnfantDAO::getAllById($idSalarie);
        $this->vue->setEnfants($lesEnfants);
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
    
      /** controleur= salarié & action=modifierEnfant & id=identifiant_enfant
     * modifier un enfant d'après son identifiant     */
    public function modifierEnfant(){
        
        $idEnfant = $_GET["idEnfant"];
        $laVue = new VueModifEnfant();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        $laVue->setUnEnfant(EnfantDAO::getOneById($idEnfant));
        $laVue->setActionRecue("modifierEnfant");
        $laVue->setActionAEnvoyer("validerModifierEnfant");
        $laVue->setMessage("Modifier l'enfant : " . $laVue->getUnEnfant()->getId());
        parent::controlerVueAdmin();
        $this->vue->setTitre("CSE La Joliverie - Salariés");
        $this->vue->afficher();
    }
    
      /** controleur= salarié & action=modifierEnfant & id=identifiant_enfant
     * valider la modification d'un enfant d'après son identifiant     */
     public function validerModifierEnfant() {
        Bdd::connecter();
         $unEnfant = new Enfant($_REQUEST['idEnfant'], $_REQUEST['nom'], ($_REQUEST['prenom']), $_REQUEST['dateNaissance']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications
            EnfantDAO::update($unEnfant->getId(), $unEnfant);
            // revenir à la liste des Groupes
           header("Location: index.php?controleur=salaries&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueModifEnfant();
            $this->vue = $laVue;
            $laVue->setUnSalarie($unEnfant);
            $laVue->setActionRecue("modifierEnfant");
            $laVue->setActionAEnvoyer("validerModifierEnfant");
            $laVue->setMessage("Modifier l'enfant : " . $laVue->getUnEnfant()->getNom()
                    . " (" . $laVue->getUnEnfant()->getNom() . ")");
            $this->vue->setTitre("CSE La Joliverie - Salariés");
            parent::controlerVueAdmin();
            $this->vue->afficher();
        }
    }
    
    /** controleur= salaries & action= generer
     * Générer la liste des salariés et des cadeaux en pdf      */
    function genererPanier() {
        Bdd::connecter();
        $_SESSION['unSalariePanier']=$unSalariePanier = SalarieDAO::getAllPanier();
        $_SESSION['unSalarieCountPanier']=$unSalarieCountPanier = SalarieDAO::getAllCountPanier();
        
        $laVue = new VueGenererPanierPDF();
        //var_dump($unSalarieCountPanier);
        //var_dump($_SESSION);
    }
    
     /** controleur= salaries & action= generer
     * Générer la liste des salariés et des cadeaux en pdf      */
    function genererJouet() {
        Bdd::connecter();
        $_SESSION['unSalarieJouet']=$unSalarieJouet = SalarieEnfantDAO::getAllJouet();
        $_SESSION['unSalarieCountJouet']=$unSalarieCountJouet = SalarieDAO::getAllCountJouet();
        
        $laVue = new VueGenererJouetPDF();
    }
    
     /** controleur= salaries & action= generer
     * Générer la liste des salariés et des cadeaux en pdf      */
    function genererCheque() {
        Bdd::connecter();
        $_SESSION['unSalarieCheque']=$unSalarieCheque = SalarieEnfantDAO::getAllCheque();
        $_SESSION['unSalarieCountCheque']=$unSalarieCountCheque = SalarieDAO::getAllCountCheque();
        
        $laVue = new VueGenererChequePDF();
    }
    
    /** Mentions légales accessibles à l'admin */
     function mentions(){
        $this->vue = new VueMentions();
        $this->vue->setTitre("CSE La Joliverie - Mentions Légales");
        parent::controlerVueAdmin();
        $this->vue->afficher();

    }
    /** controleur= salaries & action= generer
     * Générer la liste des salariés et des cadeaux en pdf      */
    function genererJouetCSV() {
        Bdd::connecter();
        $_SESSION['unSalarieJouet']=$unSalarieJouet = SalarieEnfantDAO::getAllJouetCSV();
        $_SESSION['unSalarieCountJouet']=$unSalarieCountJouet = SalarieDAO::getAllCountJouet();
        
        $laVue = new VueGenererJouetCSV();
    }
}
