<?php
namespace modele\dao;

use modele\metier\AnneeEnfant;
use PDO;

/**
 * Description of UtilisateurDAO
 * @author Melodie/Tom
 * @version 2021
 */
class AnneeEnfantDAO {
    
     protected static function metierVersEnreg(AnneeEnfant $objetMetier, $stmt) {
        $stmt->bindValue(':IDENFANT', $objetMetier->getIdEnfant());
        $stmt->bindValue(':IDCADEAU', $objetMetier->getIdCadeau());
        } 
    public static function insert(AnneeEnfant $objet) {
        $requete = "INSERT INTO ANNEE_ENFANT (`IDENFANT`, `IDCADEAU`, `ANNEE`) VALUES (:IDENFANT, :IDCADEAU, YEAR(CURRENT_DATE))";              
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->execute();
        }
    public static function isAnExistingId($idAnneeEnfant) {
        $requete = "SELECT COUNT(*) FROM ANNEE_ENFANT WHERE IDENFANT=:idAnneeEnfant AND ANNEE = YEAR(CURRENT_DATE)";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':idAnneeEnfant', $idAnneeEnfant);
        $stmt->execute();
        return $stmt->fetchColumn(0);
        }
}