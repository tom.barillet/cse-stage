<?php
namespace modele\dao;

use modele\metier\AnneeParent;
use PDO;

/**
 * Description of UtilisateurDAO
 * @author Melodie/Tom
 * @version 2021
 */
class AnneeParentDAO {
    
    
     protected static function metierVersEnreg(AnneeParent $objetMetier, $stmt) {
        $stmt->bindValue(':IDSALARIE', $objetMetier->getIdSalarie());
        $stmt->bindValue(':IDCADEAU', $objetMetier->getIdCadeau());
     }
    
    public static function insert(AnneeParent $objet) {
        $requete = "INSERT INTO ANNEE_SALARIE (`IDSALARIE`, `IDCADEAU`, `ANNEE`) VALUES (:IDSALARIE, :IDCADEAU, YEAR(CURRENT_DATE))";              
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->execute();
        }
    public static function isAnExistingId($idAnneeSalarie) {
        $requete = "SELECT COUNT(*) FROM ANNEE_SALARIE WHERE IDSALARIE=:idAnneeSalarie AND ANNEE = YEAR(CURRENT_DATE)";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':idAnneeSalarie', $idAnneeSalarie);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
    
    
    
    
    
    
    
    
    
}