<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace modele\dao;
use modele\metier\Cadeau;
use PDO;


class CadeauDAO {
   
  protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDCADEAU'];
        $nom = $enreg['LIBELLECADEAU'];
        $objetMetier = new Cadeau($id, $nom);
        return $objetMetier;
    }
    
    public static function getOneByIdEnfant($id) {
        $objetConstruit = null;
        $requete = "SELECT CN.IDCADEAU, CN.LIBELLECADEAU FROM CADEAU_NOEL CN INNER JOIN ANNEE_ENFANT ANE ON CN.IDCADEAU = ANE.IDCADEAU WHERE ANE.IDENFANT =:IDENFANT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDENFANT', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

  }