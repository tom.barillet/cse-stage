<?php
namespace modele\dao;

use modele\metier\Contact;
use PDO;

/**
 * Description of ContactDAO
 * @author Melodie/Tom
 * @version 2021
 */
class ContactDAO {
    
    
     protected static function metierVersEnreg(Contact $objetMetier, $stmt) {
        $stmt->bindValue(':IDCONTACT', $objetMetier->getIdContact());
        $stmt->bindValue(':MAILCONTACT', $objetMetier->getMailContact());
     }
        protected static function enregVersMetier(array $enreg) {
        $idContact = $enreg['IDCONTACT'];
        $mailContact = $enreg['MAILCONTACT'];
        
        $objetMetier = new Contact($idContact, $mailContact);
        return $objetMetier;
    }
    
    public static function insert(Contact $objet) {
        $requete = "INSERT INTO CONTACT (`IDCONTACT`, `MAILCONTACT`) VALUES (:IDCONTACT, :MAILCONTACT)";              
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->execute();
        }
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM CONTACT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
         /**
     * Permet de vérifier s'il existe ou non un Salarié ayant déjà le même mail dans la BD
     * @param string $unMail identifiant du salarié à tester
     * @return boolean =true si le mail existe déjà, =false sinon
     */
    public static function isAnExistingMail($unMail) {
        $requete = "SELECT COUNT(*) FROM CONTACT WHERE MAILCONTACT=:ADRESSEMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ADRESSEMAIL', $unMail);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }    
      public static function deleteWithEmail($unMail) {
        $ok = false;
        $requete = "DELETE C FROM CONTACT C WHERE C.MAILCONTACT = :MAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':MAIL', $unMail);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
    
}