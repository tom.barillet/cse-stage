<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace modele\dao;
use modele\metier\Email;
use PDO;


class EmailDAO {
    
/**
     * crée un objet métier à partir d'un enregistrement
     * @param array $enreg
     * @return Email objet métier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $idEmail = $enreg['IDEMAIL'];
        $email = $enreg['EMAIL'];
        
        $objetMetier = new Email($idEmail, $email);
        return $objetMetier;
    }
        
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Enfant
     * @param Email $objetMetier un Email
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Email $objetMetier, $stmt) {
        $stmt->bindValue(':IDEMAIL', $objetMetier->getIdEmail());
        $stmt->bindValue(':EMAIL', $objetMetier->getEmail());
    }
    
    /**
     * Retourne la liste de tous les email 
     * @return array tableau d'objets d'email
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM EMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche un email selon la valeur de son identifiant
     * @param string $id
     * @return Email l'email trouvé ; null sinon
     */
    public static function getOneById($idEmail) {
        $objetConstruit = null;
        $requete = "SELECT * FROM EMAIL WHERE IDEMAIL = :IDEMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDEMAIL', $idEmail);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    /**
     * Recherche un utilisateur selon la valeur de son login
     * @param string $unMail valeur de login recherchée
     * @return Utilisateur l'utilisateur trouvé ; null sinon
     */
    public static function getOneByMail($unMail) {
        $objetConstruit = null;
        $requete = "SELECT * FROM EMAIL WHERE EMAIL = :EMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':EMAIL', $unMail);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    } 
    
    
     public static function isAnExistingMail($unMail) {
        $requete = "SELECT COUNT(*) FROM EMAIL WHERE EMAIL=:EMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':EMAIL', $unMail);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }  
}