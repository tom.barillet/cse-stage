<?php
namespace modele\dao;

use modele\metier\Enfant;
use PDO;

/**
 * Description of EnfantDAO
 * Classe métier  :  Enfant
 * @author Barillet/Menadier
 * @version 2021
 */
class EnfantDAO {
     /**
     * crée un objet métier à partir d'un enregistrement
     * @param array $enreg
     * @return Enfant objet métier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDENFANT'];
        $nom = $enreg['NOMENFANT'];
        $prenom = $enreg['PRENOMENFANT'];
        $dateNaiss = $enreg['DATENAISSANCE'];
        

        $objetMetier = new Enfant($id, $nom, $prenom, $dateNaiss);
        return $objetMetier;
    }
    
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Enfant
     * @param Enfant $objetMetier un Salarie
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Enfant $objetMetier, $stmt) {
        $stmt->bindValue(':IDENFANT', $objetMetier->getId());
        $stmt->bindValue(':NOMENFANT', $objetMetier->getNom());
        $stmt->bindValue(':PRENOMENFANT', $objetMetier->getPrenom());
        $stmt->bindValue(':DATENAISSANCE', $objetMetier->getDateNaissance());
    }
    
    protected static function metierVersEnregSansId(Enfant $objetMetier, $stmt) {
        $stmt->bindValue(':NOMENFANT', $objetMetier->getNom());
        $stmt->bindValue(':PRENOMENFANT', $objetMetier->getPrenom());
        $stmt->bindValue(':DATENAISSANCE', $objetMetier->getDateNaissance());
    }
    
    /**
     * Retourne la liste de tous les enfants 
     * @return array tableau d'objets d' enfants
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM ENFANT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    public static function getAllById($id) {
        $lesObjets = array();
        $requete = "SELECT E.IDENFANT, E.NOMENFANT, E.PRENOMENFANT, E.DATENAISSANCE FROM ENFANT E INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT INNER JOIN SALARIE S on S.IDSALARIE = F.IDSALARIE WHERE S.IDSALARIE = :ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Enfant $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Enfant $objet) {
        $requete = "INSERT INTO ENFANT (NOMENFANT, PRENOMENFANT, DATENAISSANCE) VALUES (:NOMENFANT, :PRENOMENFANT, :DATENAISSANCE);";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnregSansId($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
      public static function getIdByEnfant($nom, $prenom, $date) {
        $lesObjets = null;
        $requete = "SELECT * FROM ENFANT WHERE NOMENFANT =:NOMENFANT AND PRENOMENFANT =:PRENOMENFANT AND DATENAISSANCE =:DATE LIMIT 1";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':NOMENFANT', $nom);
        $stmt->bindParam(':PRENOMENFANT', $prenom);
        $stmt->bindParam(':DATE', $date);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
     public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM ENFANT WHERE IDENFANT = :IDENFANT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDENFANT', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    public static function getOneAgeById($id) {
        $requete = "SELECT YEAR(CURRENT_DATE) - YEAR(DATENAISSANCE) FROM ENFANT WHERE IDENFANT=:IDENFANT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDENFANT', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
         public static function update($idEnfant, Enfant $objet) {
        $ok = false;
        $requete = "UPDATE ENFANT SET NOMENFANT=:NOMENFANT, PRENOMENFANT=:PRENOMENFANT, DATENAISSANCE=:DATENAISSANCE WHERE IDENFANT=:IDENFANT;";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
      public static function delete($id) {
        $ok = false;
        $requete = "DELETE F,E FROM ENFANT E INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT WHERE E.IDENFANT =:IDENFANT";        
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDENFANT', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    
    
}

