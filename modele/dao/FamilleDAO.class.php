<?php
namespace modele\dao;

use modele\metier\Famille;
use modele\metier\Salarie;
use modele\dao\SalarieDAO;
use modele\metier\Enfant;
use modele\dao\EnfantDAO;
use PDO;

/**
 * Description of FamilleDAO
 * Classe métier  :  Famille
 * @author Barillet/Menadier
 * @version 2021
 */
class FamilleDAO {
     /**
     * crée un objet métier à partir d'un enregistrement
     * @param array $enreg
     * @return Famille objet métier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $idSalarie = $enreg['IDSALARIE'];
        $idEnfant = $enreg['IDENFANT'];
        
        $objetSalarie = SalarieDAO::getOneById($idSalarie);
        $objetEnfant = EnfantDAO::getOneById($idEnfant);
        
        $objetMetier = new Famille($objetSalarie, $objetEnfant);
        return $objetMetier;
    }
    
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Famille
     * @param Famille $objetMetier un Famille
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Famille $objetMetier, $stmt) {
        $stmt->bindValue(':IDSALARIE', $objetMetier->getIdSalarie()->getId());
        $stmt->bindValue(':IDENFANT', $objetMetier->getIdEnfant()->getId());
    }
    
    /**
     * Retourne la liste de toutes les familles 
     * @return array tableau d'objets des familles
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM FAMILLE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    public static function getAllById($idSalarie) {
        $lesObjets = array();
        $requete = "SELECT * FROM FAMILLE F INNER JOIN SALARIE S ON F.IDSALARIE = S.IDSALARIE INNER JOIN ENFANT E ON F.IDENFANT = E.IDENFANT WHERE F.IDSALARIE = :IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDSALARIE', $idSalarie);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
     public static function getAllByIdEnfant($idEnfant) {
        $lesObjets = array();
        $requete = "SELECT S.NOM, S.PRENOM, E.NOM, E.PRENOM FROM FAMILLE F INNER JOIN SALARIE S ON F.IDSALARIE = S.IDSALARIE INNER JOIN ENFANT ON F.IDENFANT = E.IDENFANT WHERE F.IDENFANT = :IDENFANT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDENFANT', $idEnfant);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
     /**
     * Recherche une famille selon la valeur de son identifiant salarié
     * @param string $idSalarie
     * @return Salarié le salarié trouvé ; null sinon
     */
    public static function getOneByIdSalarie($idSalarie) {
        $objetConstruit = null;
        $requete = "SELECT * FROM FAMILLE WHERE IDSALARIE = :IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDSALARIE', $idSalarie);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    } 
    /**
     * Recherche une famille selon la valeur de son identifiant enfant
     * @param string $idEnfant
     * @return Salarié le salarié trouvé ; null sinon
     */
    public static function getOneByIdEnfant($idEnfant) {
        $objetConstruit = null;
        $requete = "SELECT * FROM FAMILLE WHERE IDENFANT = :IDENFANT";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDENFANT', $idEnfant);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Famille $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Famille $objet) {
        $requete = "INSERT INTO FAMILLE (IDSALARIE, IDENFANT) VALUES (:IDSALARIE, :IDENFANT);";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
       /**
     * Permet de v�rifier s'il existe ou non un Salari� ayant d�j� le m�me identifiant dans la BD
     * @param string $idSalarie identifiant du salari� � tester
     * @return boolean =true si l'id existe d�j�, =false sinon
     */
    public static function isAnExistingId($idSalarie) {
        $requete = "SELECT COUNT(*) FROM FAMILLE WHERE IDSALARIE=:IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDSALARIE', $idSalarie);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
}

