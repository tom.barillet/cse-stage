<?php
namespace modele\dao;

use modele\metier\Salarie;
use modele\dao\SiteDAO;
use PDO;

/**
 * Description of SalarieDAO
 * Classe m�tier  :  Salarie
 * @author Barillet/Menadier
 * @version 2021
 */
class SalarieDAO {
    /**
     * cr�e un objet m�tier � partir d'un enregistrement
     * @param array $enreg
     * @return Salarie objet m�tier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDSALARIE'];
        $idSite = $enreg['IDSITE'];
        $email = $enreg['ADRESSEMAIL'];
        $nom = $enreg['NOM'];
        $prenom = $enreg['PRENOM'];
        $estOgec = $enreg['ESTOGEC'];
        $mdp = $enreg['MOTDEPASSE'];
        $estAdmin = $enreg['ESTADMIN'];
        
        $objetSite = SiteDAO::getOneById($idSite);

        $objetMetier = new Salarie($id, $objetSite, $email, $nom, $prenom, $estOgec, $mdp, $estAdmin);
        return $objetMetier;
    }
    
    /**
     * Valorise les param�tres d'une requ�te pr�par�e avec l'�tat d'un objet Salarie
     * @param Salarie $objetMetier un Salarie
     * @param PDOStatement $stmt requ�te pr�par�e
     */
    protected static function metierVersEnreg(Salarie $objetMetier, $stmt) {
        $stmt->bindValue(':IDSALARIE', $objetMetier->getId());
        $stmt->bindValue(':IDSITE', $objetMetier->getIdSite()->getId());
        $stmt->bindValue(':ADRESSEMAIL', $objetMetier->getEmail());
        $stmt->bindValue(':NOM', $objetMetier->getNom());
        $stmt->bindValue(':PRENOM', $objetMetier->getPrenom());
        $stmt->bindValue(':ESTOGEC', $objetMetier->getOgec());
        $stmt->bindValue(':MOTDEPASSE', $objetMetier->getMdp());
        $stmt->bindValue(':ESTADMIN', $objetMetier->getAdmin());
    }
    
    protected static function metierVersEnregSansId(Salarie $objetMetier, $stmt) {
        $stmt->bindValue(':IDSITE', $objetMetier->getIdSite()->getId());
        $stmt->bindValue(':ADRESSEMAIL', $objetMetier->getEmail());
        $stmt->bindValue(':NOM', $objetMetier->getNom());
        $stmt->bindValue(':PRENOM', $objetMetier->getPrenom());
        $stmt->bindValue(':ESTOGEC', $objetMetier->getOgec());
        $stmt->bindValue(':MOTDEPASSE', $objetMetier->getMdp());
        $stmt->bindValue(':ESTADMIN', $objetMetier->getAdmin());
    }
    
  /**
     * Recherche un utilisateur selon la valeur de son login
     * @param string $unMail valeur de login recherch�e
     * @return Utilisateur l'utilisateur trouv� ; null sinon
     */
    public static function getOneByMail($unMail) {
        $objetConstruit = null;
        $requete = "SELECT * FROM SALARIE WHERE ADRESSEMAIL = :ADRESSEMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ADRESSEMAIL', $unMail);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }  
 /**
     * Mettre � jour enregistrement dans la table � partir de l'�tat d'un objet m�tier
     * @param string identifiant de l'enregistrement � mettre � jour
     * @param Salarie $objet objet m�tier � mettre � jour
     * @return boolean =FALSE si l'op�rationn �choue
     */
        public static function update($idSalarie, Salarie $objet) {
        $ok = false;
        $requete = "UPDATE SALARIE SET IDSITE=:IDSITE, ADRESSEMAIL=:ADRESSEMAIL, NOM=:NOM, PRENOM=:PRENOM, ESTOGEC=:ESTOGEC, MOTDEPASSE=:MOTDEPASSE, ESTADMIN=:ESTADMIN WHERE IDSALARIE=:IDSALARIE;";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
     /**
     * Retourne la liste de tous les salari�s 
     * @return array tableau d'objets de salari�
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM SALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche un salari� selon la valeur de son identifiant
     * @param string $id
     * @return Salari� le salari� trouv� ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM SALARIE WHERE IDSALARIE = :IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDSALARIE', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
      /**
     * D�truire un enregistrement de la table GROUPE d'apr�s son identifiant
     * @param string identifiant de l'enregistrement � d�truire
     * @return boolean =TRUE si l'enregistrement est d�truit, =FALSE si l'op�ration �choue
     */
    public static function deleteWithChild($id) {
        $ok = false;
        $requete = "DELETE F,E,S FROM SALARIE S INNER JOIN FAMILLE F on F.IDSALARIE = S.IDSALARIE INNER JOIN ENFANT E ON E.IDENFANT = F.IDENFANT WHERE S.IDSALARIE = :ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
     public static function deleteWithoutChild($id) {
        $ok = false;
        $requete = "DELETE S FROM SALARIE S WHERE S.IDSALARIE = :ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
    /**
     * Insérer un nouvel enregistrement dans la table � partir de l'�tat d'un objet m�tier
     * @param Salarie $objet objet m�tier � ins�rer
     * @return boolean =FALSE si l'op�ration �choue
     */
    public static function insert(Salarie $objet) {
        $requete = "INSERT INTO SALARIE (`IDSITE`, `ADRESSEMAIL`, `NOM`, `PRENOM`, `ESTOGEC`, `MOTDEPASSE`, `ESTADMIN`) VALUES (:IDSITE, :ADRESSEMAIL, :NOM, :PRENOM, :ESTOGEC, :MOTDEPASSE, :ESTADMIN);";              
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnregSansId($objet, $stmt);
        $stmt->execute();
        }
    
    
     /**
     * Permet de v�rifier s'il existe ou non un Salari� ayant d�j� le m�me identifiant dans la BD
     * @param string $idSalarie identifiant du salari� � tester
     * @return boolean =true si l'id existe d�j�, =false sinon
     */
    public static function isAnExistingId($idSalarie) {
        $requete = "SELECT COUNT(*) FROM SALARIE WHERE IDSALARIE=:IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':IDSALARIE', $idSalarie);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
       /**
     * Permet de vérifier s'il existe ou non un Salarié ayant déjà le même nom dans la BD
     * @param string $nom identifiant du salarié à tester
     * @return boolean =true si le nom existe déjà, =false sinon
     */
    public static function isAnExistingNom($nom) {
        $requete = "SELECT COUNT(*) FROM SALARIE WHERE NOM=:NOM";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':NOM', $nom);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
        /**
     * Permet de vérifier s'il existe ou non un Salarié ayant déjà le même mail dans la BD
     * @param string $unMail identifiant du salarié à tester
     * @return boolean =true si le mail existe déjà, =false sinon
     */
    public static function isAnExistingMail($unMail) {
        $requete = "SELECT COUNT(*) FROM SALARIE WHERE ADRESSEMAIL=:ADRESSEMAIL";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ADRESSEMAIL', $unMail);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }    
    
     /**
     * Retourne la liste de tous les salariés qui ont pris un panier garnis
     * @return array tableau d'objets de salarié
     */
    public static function getAllPanier() {
        $lesObjets = array();
        $requete = "SELECT * FROM SALARIE S INNER JOIN ANNEE_SALARIE ANS on ANS.IDSALARIE = S.IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
        /**
     * Retourne la liste de tous les enfants des salariés qui ont pris un jouet
     * @return array tableau d'objets 
     */
    public static function getAllJouet() {
        $lesObjets = array();
        $requete = "SELECT * FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Jouet'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Retourne la liste de tous les enfants des salariés qui ont pris un cheque
     * @return array tableau d'objets 
     */
    public static function getAllCheque() {
        $lesObjets = array();
        $requete = "SELECT * FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Chèque Kadéos'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Retourne le nombre de tous les salariés qui ont pris un cheque
     * @return array tableau d'objets 
     */
     public static function getAllCountPanier() {
        $requete = "SELECT COUNT(*)FROM SALARIE S INNER JOIN ANNEE_SALARIE ANS on ANS.IDSALARIE = S.IDSALARIE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    } 
    
    /**
     * Retourne le nombre de tous les enfants des salariés qui ont pris un jouet
     * @return array tableau d'objets 
     */
     public static function getAllCountJouet() {
        $requete = "SELECT COUNT(*) FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Jouet'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    } 
    
    /**
     * Retourne le nombre de tous les enfants des salariés qui ont pris un cheque
     * @return array tableau d'objets 
     */
     public static function getAllCountCheque() {
        $requete = "SELECT COUNT(*) FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Chèque Kadéos'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
   
  
}
