<?php
namespace modele\dao;

use modele\metier\SalarieEnfant;
use modele\dao\SiteDAO;
use PDO;

/**
 * Description of SalarieEnfantDAO
 * Classe métier  :  SalarieEnfant
 * @author Barillet/Menadier
 * @version 2021
 */
class SalarieEnfantDAO {
    /**
     * cr�e un objet métier à partir d'un enregistrement
     * @param array $enreg
     * @return Salarie objet métier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDSALARIE'];
        $idSite = $enreg['IDSITE'];
        $email = $enreg['ADRESSEMAIL'];
        $nom = $enreg['NOM'];
        $prenom = $enreg['PRENOM'];
        $estOgec = $enreg['ESTOGEC'];
        $mdp = $enreg['MOTDEPASSE'];
        $estAdmin = $enreg['ESTADMIN'];
        $idEnfant = $enreg['IDENFANT'];
        $nomEnfant = $enreg['NOMENFANT'];
        $prenomEnfant = $enreg['PRENOMENFANT'];
        $dateNaiss = $enreg['DATENAISSANCE'];
        
        $objetSite = SiteDAO::getOneById($idSite);

        $objetMetier = new SalarieEnfant($id, $objetSite, $email, $nom, $prenom, $estOgec, $mdp, $estAdmin, $idEnfant, $nomEnfant, $prenomEnfant, $dateNaiss);
        return $objetMetier;
    }
    
    /**
     * Valorise les param�tres d'une requ�te pr�par�e avec l'�tat d'un objet SalarieEnfant
     * @param Salarie $objetMetier un SalarieEnfant
     * @param PDOStatement $stmt requ�te pr�par�e
     */
    protected static function metierVersEnreg(SalarieEnfant $objetMetier, $stmt) {
        $stmt->bindValue(':IDSALARIE', $objetMetier->getId());
        $stmt->bindValue(':IDSITE', $objetMetier->getIdSite()->getId());
        $stmt->bindValue(':ADRESSEMAIL', $objetMetier->getEmail());
        $stmt->bindValue(':NOM', $objetMetier->getNom());
        $stmt->bindValue(':PRENOM', $objetMetier->getPrenom());
        $stmt->bindValue(':ESTOGEC', $objetMetier->getOgec());
        $stmt->bindValue(':MOTDEPASSE', $objetMetier->getMdp());
        $stmt->bindValue(':ESTADMIN', $objetMetier->getAdmin());
        $stmt->bindValue(':IDENFANT', $objetMetier->getIdEnfant());
        $stmt->bindValue(':NOMENFANT', $objetMetier->getNomEnfant());
        $stmt->bindValue(':PRENOMENFANT', $objetMetier->getPrenomEnfant());
        $stmt->bindValue(':DATENAISSANCE', $objetMetier->getDateNaissance());
    }
    
    /**
     * Retourne la liste de tous les enfants des salariés qui ont pris un jouet
     * @return array tableau d'objets 
     */
    public static function getAllJouet() {
        $lesObjets = array();
        $requete = "SELECT * FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Jouet'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
       /**
     * Retourne la liste de tous les enfants des salariés qui ont pris un jouet
     * @return array tableau d'objets 
     */
    public static function getAllJouetCSV() {
        $lesObjets = array();
        $requete = "SELECT S.NOM, S.PRENOM, S.ADRESSEMAIL, E.NOMENFANT, E.PRENOMENFANT, E.DATENAISSANCE FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Jouet'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] =$enreg;
            }
        }
        return $lesObjets;
    }
    
    
    /**
     * Retourne la liste de tous les enfants des salariés qui ont pris un cheque
     * @return array tableau d'objets 
     */
    public static function getAllCheque() {
        $lesObjets = array();
        $requete = "SELECT * FROM ENFANT E 
INNER JOIN ANNEE_ENFANT ANE on ANE.IDENFANT = E.IDENFANT
INNER JOIN CADEAU_NOEL CN on CN.IDCADEAU = ANE.IDCADEAU
INNER JOIN FAMILLE F on F.IDENFANT = E.IDENFANT
INNER JOIN SALARIE S ON S.IDSALARIE = F.IDSALARIE
WHERE CN.LIBELLECADEAU = 'Chèque Kadéos'";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

}
