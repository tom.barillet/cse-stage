<?php

namespace modele\dao;

use modele\metier\Site;
use PDO;

/**
 * Description of SiteDAO
 * Classe métier :  Site
 * @author Barillet/Menadier
 * @version 2021
 */
class SiteDAO {
    
    /**
     * crée un objet prestation à partir d'un enregistrement
     * @param array $enreg
     * @return Lieu objet prestation obtenu
     */
     protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDSITE'];
        $nom = $enreg['NOMSITE'];
        $objetMetier = new Site($id, $nom);
        return $objetMetier;
    }
    
    /**
     * Retourne la liste de tous les lieux 
     * @return array tableau d'objets de lieu
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM SITE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $id
     * @return Lieu le lieu trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM SITE WHERE IDSITE = :ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
}
