<?php
namespace modele\metier;

/**
 * Description of Enfant
 */
class AnneeEnfant {
    
    private $idEnfant;
    private $idCadeau;
    private $annee;
    
    function __construct($idEnfant, $idCadeau, $annee) {
        $this->idEnfant = $idEnfant;
        $this->idCadeau = $idCadeau;
        $this->annee = $annee;
    }
     function getIdEnfant() {
        return $this->idEnfant;
    }

    function getIdCadeau() {
        return $this->idCadeau;
    }

    function getAnnee() {
        return $this->annee;
    }
    function setIdEnfant($idEnfant) {
        $this->idEnfant = $idEnfant;
    }
    
    function setIdCadeau($idCadeau) {
        $this->idCadeau = $idCadeau;
    }

    function setAnnee($annee) {
        $this->annee = $annee;
    }

    
    
}
