<?php
namespace modele\metier;

/**
 * Description of Enfant
 */
class AnneeParent {
    
    private $idSalarie;
    private $idCadeau;
    private $annee;
    
    function __construct($idSalarie, $idCadeau, $annee) {
        $this->idSalarie = $idSalarie;
        $this->idCadeau = $idCadeau;
        $this->annee = $annee;
    }
     function getIdSalarie() {
        return $this->idSalarie;
    }

    function getIdCadeau() {
        return $this->idCadeau;
    }

    function getAnnee() {
        return $this->annee;
    }
    function setIdSalarie($idSalarie) {
        $this->idSalarie = $idSalarie;
    }
    
    function setIdCadeau($idCadeau) {
        $this->idCadeau = $idCadeau;
    }

    function setAnnee($annee) {
        $this->annee = $annee;
    }

    
    
}
