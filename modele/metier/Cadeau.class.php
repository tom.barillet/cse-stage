<?php
namespace modele\metier;

/**
 * Description of Cadeau
 */
class Cadeau {
    
    private $idCadeau;
    private $libelleCadeau;
    
    function __construct($idCadeau, $libelleCadeau) {
        $this->idCadeau = $idCadeau;
        $this->libelleCadeau = $libelleCadeau;
    }

    function getIdCadeau() {
        return $this->idCadeau;
    }

    function getLibelleCadeau() {
        return $this->libelleCadeau;
    }
    
    function setIdCadeau($idCadeau) {
        $this->idCadeau = $idCadeau;
    }

    function setLibelleCadeau($libelle) {
        $this->libelleCadeau = $libelle;
    }

    
    
}
