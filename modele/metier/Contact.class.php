<?php
namespace modele\metier;

/**
 * Description of Enfant
 */
class Contact {
    
    private $idContact;
    private $mailContact;
    
    function __construct($idContact, $mailContact) {
        $this->idContact = $idContact;
        $this->mailContact = $mailContact;
    }
    
    function getIdContact() {
        return $this->idContact;
    }

    function getMailContact() {
        return $this->mailContact;
    }

    function setIdContact($idContact): void {
        $this->idContact = $idContact;
    }

    function setMailContact($mailContact): void {
        $this->mailContact = $mailContact;
    }


    
    
}
