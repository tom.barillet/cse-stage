<?php
namespace modele\metier;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Email{

    private $idEmail;

    private $email;

    function __construct($idEmail, $email) {
        $this->idEmail = $idEmail;
        $this->email = $email;
    }
    
    function getIdEmail() {
        return $this->idEmail;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdEmail($idEmail): void {
        $this->idEmail = $idEmail;
    }

    function setEmail($email): void {
        $this->email = $email;
    }


}