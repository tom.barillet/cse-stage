<?php
namespace modele\metier;

/**
 * Description of Enfant
 */
class Enfant {
    private $id;
    private $nom;
    private $prenom;
    private $dateNaissance;
    
    function __construct($id, $nom, $prenom, $dateNaissance) {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->dateNaissance = $dateNaissance;
    }
    public function __toString() {
        return get_class($this). "{ id=". $this->id 
                . " prenom=". $this->prenom . " nom=" . $this->nom
                . "- email=". $this->dateNaissance . "- date de naissance= ". "}";
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getDateNaissance() {
        return $this->dateNaissance;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setDateNaissance($dateNaiss) {
        $this->dateNaissance = $dateNaiss;
    }

}
