<?php
namespace modele\metier;

/**
 * Description of Famille
 */
class Famille {
    private $idSalarie;
    private $idEnfant;
    
    function __construct(Salarie $idSalarie, Enfant $idEnfant) {
        $this->idSalarie = $idSalarie;
        $this->idEnfant = $idEnfant;
    }

    function getIdSalarie() : Salarie{
        return $this->idSalarie;
    }

    function getIdEnfant() : Enfant{
        return $this->idEnfant;
    }

    function setIdSalarie(Salarie $idSalarie): void {
        $this->idSalarie = $idSalarie;
    }

    function setIdEnfant(Enfant $idEnfant): void {
        $this->idEnfant = $idEnfant;
    }


}