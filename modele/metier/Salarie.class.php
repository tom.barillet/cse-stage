<?php
namespace modele\metier;

/**
 * Description of Salarie
 *
 * @author Barillet/Menadier
 * Version 2021
 */
class Salarie {
    private $id;
    private $nom;
    private $prenom;
    private $email;
    private $mdp;
    private $estOgec;
    private $estAdmin;
    private $idSite;
    
    function __construct($id, Site $idSite ,$email, $nom, $prenom, $estOgec, $mdp, $estAdmin) {
        $this->id = $id;
        $this->idSite = $idSite;
        $this->email = $email;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->estOgec = $estOgec;
        $this->mdp = $mdp;
        $this->estAdmin = $estAdmin;
        
    }

    public function __toString() {
        return get_class($this). "{ id=". $this->id 
                . " prenom=". $this->prenom . " nom=" . $this->nom
                . "- email=". $this->email . "- mdp= ". $this->mdp . "}";
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getEmail() {
        return $this->email;
    }

    function getMdp() {
        return $this->mdp;
    }
    
    function getIdSite(): Site {
        return $this->idSite;
    }
    
     function getOgec() {
        return $this->estOgec;
    }
    
      function getAdmin() {
        return $this->estAdmin;
    }


    function setId($id) {
        $this->id = $id;
    }
    
    function setNom($nom) {
        $this->nom = $nom;
    }

    function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setMdp($mdp) {
        $this->mdp = $mdp;
    }
    
    function setIdSite(Site $idSite) {
        $this->idSite = $idSite;
    }
    
    function getLogin() {
        return $this->login;
    }

    function setLogin($login) {
        $this->login = $login;
    }


}
