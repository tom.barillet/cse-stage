<?php
namespace modele\metier;
use modele\metier\Site;

/**
 * Description of SalarieEnfant
 *Création d'une classe métier qui pourra afficher le nom du salarié et de ses enfants
 * @author Barillet/Menadier
 * Version 2021
 */
class SalarieEnfant {
    private $id;
    private $unSite;
    private $email;
    private $nom;
    private $prenom;
    private $estOgec;
    private $mdp;
    private $estAdmin;
    private $idEnfant;
    private $nomEnfant;
    private $prenomEnfant;
    private $dateNaissance;
    
    function __construct($id, Site $unSite, $email, $nom, $prenom, $estOgec, $mdp, $estAdmin, $idEnfant, $nomEnfant, $prenomEnfant, $dateNaissance) {
        $this->id = $id;
        $this->unSite = $unSite;
        $this->email = $email;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->estOgec = $estOgec;
        $this->mdp = $mdp;
        $this->estAdmin = $estAdmin;
        $this->idEnfant = $idEnfant;
        $this->nomEnfant = $nomEnfant;
        $this->prenomEnfant = $prenomEnfant;
        $this->dateNaissance = $dateNaissance;
    }

    
    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    function getEmail() {
        return $this->email;
    }

    function getMdp() {
        return $this->mdp;
    }

    function getEstOgec() {
        return $this->estOgec;
    }

    function getEstAdmin() {
        return $this->estAdmin;
    }

    function getunSite(): Site {
        return $this->unSite;
    }

    function getIdEnfant() {
        return $this->idEnfant;
    }

    function getNomEnfant() {
        return $this->nomEnfant;
    }

    function getPrenomEnfant() {
        return $this->prenomEnfant;
    }

    function getDateNaissance() {
        return $this->dateNaissance;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setNom($nom): void {
        $this->nom = $nom;
    }

    function setPrenom($prenom): void {
        $this->prenom = $prenom;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setMdp($mdp): void {
        $this->mdp = $mdp;
    }

    function setEstOgec($estOgec): void {
        $this->estOgec = $estOgec;
    }

    function setEstAdmin($estAdmin): void {
        $this->estAdmin = $estAdmin;
    }

    function setUnSite(Site $unSite) {
        $this->unSite = $unSite;
    }

    function setIdEnfant($idEnfant): void {
        $this->idEnfant = $idEnfant;
    }

    function setNomEnfant($nomEnfant): void {
        $this->nomEnfant = $nomEnfant;
    }

    function setPrenomEnfant($prenomEnfant): void {
        $this->prenomEnfant = $prenomEnfant;
    }

    function setDateNaissance($dateNaissance): void {
        $this->dateNaissance = $dateNaissance;
    }



    
}