<?php
namespace modele\metier;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Site{
    
    /**
     * id du lieu
     * @var string
     */
    private $id;
    
    /**
     * nom du lieu
     * @var string
     */
    private $nom;
    
     // constructeur
    function __construct(string $id, string $nom) {
        $this->id = $id;
        $this->nom = $nom;
    }

    
     function getId(): string {
        return $this->id;
    }
 
    function getNom(): string {
        return $this->nom;
    }

    function setNom(string $nom) {
        $this->nom = $nom;
    }

    function setId(string $id) {
        $this->id = $id;
    }
    
}