DROP DATABASE mmenadier_BDD_CE;
CREATE DATABASE IF NOT EXISTS mmenadier_BDD_CE;
USE mmenadier_BDD_CE;
# -----------------------------------------------------------------------------
#       SUPRESSION DES TABLES SI ELLES EXISTENT
# -----------------------------------------------------------------------------
DROP TABLE IF EXISTS ENFANT;

DROP TABLE IF EXISTS SALARIE;

DROP TABLE IF EXISTS CADEAU_NOEL;

DROP TABLE IF EXISTS SITE;

DROP TABLE IF EXISTS FAMILLE;

DROP TABLE IF EXISTS ANNEE_SALARIE;

DROP TABLE IF EXISTS ANNEE_ENFANT;

DROP TABLE IF EXISTS EMAIL;

DROP TABLE IF EXISTS CONTACT;

# -----------------------------------------------------------------------------
#       TABLE : ENFANT
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS ENFANT
 (
   IDENFANT INTEGER(4) NOT NULL AUTO_INCREMENT,
   NOMENFANT VARCHAR(100) NULL ,
   PRENOMENFANT VARCHAR(100) NULL ,
   DATENAISSANCE DATE NULL  
   , PRIMARY KEY (IDENFANT) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       TABLE : SALARIE
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS SALARIE
 (
   IDSALARIE INTEGER(4) NOT NULL AUTO_INCREMENT ,
   IDSITE INTEGER(2) NOT NULL ,
   ADRESSEMAIL VARCHAR(100) NULL ,
   NOM VARCHAR(100) NULL ,
   PRENOM VARCHAR(100) NULL ,
   ESTOGEC BOOL NULL ,
   MOTDEPASSE VARCHAR(100) NULL ,
   ESTADMIN BOOL NULL  
   , PRIMARY KEY (IDSALARIE) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       INDEX DE LA TABLE SALARIE
# -----------------------------------------------------------------------------


CREATE  INDEX I_FK_SALARIE_SITE
     ON SALARIE (IDSITE ASC);

# -----------------------------------------------------------------------------
#       TABLE : CADEAU_NOEL
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS CADEAU_NOEL
 (
   IDCADEAU INTEGER(2) NOT NULL AUTO_INCREMENT ,
   LIBELLECADEAU VARCHAR(100) NULL  
   , PRIMARY KEY (IDCADEAU) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       TABLE : SITE
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS SITE
 (
   IDSITE INTEGER(2) NOT NULL ,
   NOMSITE VARCHAR(100) NULL  
   , PRIMARY KEY (IDSITE) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       TABLE : FAMILLE
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS FAMILLE
 (
   IDSALARIE INTEGER(4) NOT NULL ,
   IDENFANT INTEGER(4) NOT NULL  
   , PRIMARY KEY (IDSALARIE,IDENFANT) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       INDEX DE LA TABLE FAMILLE
# -----------------------------------------------------------------------------


CREATE  INDEX I_FK_FAMILLE_SALARIE
     ON FAMILLE (IDSALARIE ASC);

CREATE  INDEX I_FK_FAMILLE_ENFANT
     ON FAMILLE (IDENFANT ASC);

# -----------------------------------------------------------------------------
#       TABLE : ANNEE_SALARIE
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS ANNEE_SALARIE
 (
   IDCADEAU INTEGER(2) NOT NULL ,
   IDSALARIE INTEGER(4) NOT NULL ,
   ANNEE VARCHAR(100) NULL  
   , PRIMARY KEY (IDCADEAU,IDSALARIE) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       INDEX DE LA TABLE ANNEE_SALARIE
# -----------------------------------------------------------------------------


CREATE  INDEX I_FK_ANNEE_SALARIE_CADEAU_NOEL
     ON ANNEE_SALARIE (IDCADEAU ASC);

CREATE  INDEX I_FK_ANNEE_SALARIE_SALARIE
     ON ANNEE_SALARIE (IDSALARIE ASC);

# -----------------------------------------------------------------------------
#       TABLE : ANNEE_ENFANT
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS ANNEE_ENFANT
 (
   IDENFANT INTEGER(4) NOT NULL ,
   IDCADEAU INTEGER(2) NOT NULL ,
   ANNEE VARCHAR(100) NULL  
   , PRIMARY KEY (IDENFANT,IDCADEAU) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       INDEX DE LA TABLE ANNEE_ENFANT
# -----------------------------------------------------------------------------


CREATE  INDEX I_FK_ANNEE_ENFANT_ENFANT
     ON ANNEE_ENFANT (IDENFANT ASC);

CREATE  INDEX I_FK_ANNEE_ENFANT_CADEAU_NOEL
     ON ANNEE_ENFANT (IDCADEAU ASC);


# -----------------------------------------------------------------------------
#       TABLE : EMAIL
# -----------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS EMAIL
 (
   IDEMAIL INTEGER(4) NOT NULL AUTO_INCREMENT,
   EMAIL VARCHAR(100) NULL  
   , PRIMARY KEY (IDEMAIL) 
 ) 
 comment = "";

# -----------------------------------------------------------------------------
#       TABLE : CONTACT
# -----------------------------------------------------------------------------


CREATE TABLE IF NOT EXISTS CONTACT (
  IDCONTACT int NOT NULL AUTO_INCREMENT,
  MAILCONTACT varchar(254) NOT NULL,
  PRIMARY KEY (IDCONTACT)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
COMMIT;


# -----------------------------------------------------------------------------
#       CREATION DES REFERENCES DE TABLE
# -----------------------------------------------------------------------------


ALTER TABLE SALARIE 
  ADD FOREIGN KEY FK_SALARIE_SITE (IDSITE)
      REFERENCES SITE (IDSITE) ;


ALTER TABLE FAMILLE 
  ADD FOREIGN KEY FK_FAMILLE_SALARIE (IDSALARIE)
      REFERENCES SALARIE (IDSALARIE) ;


ALTER TABLE FAMILLE 
  ADD FOREIGN KEY FK_FAMILLE_ENFANT (IDENFANT)
      REFERENCES ENFANT (IDENFANT) ;


ALTER TABLE ANNEE_SALARIE 
  ADD FOREIGN KEY FK_ANNEE_SALARIE_CADEAU_NOEL (IDCADEAU)
      REFERENCES CADEAU_NOEL (IDCADEAU) ;


ALTER TABLE ANNEE_SALARIE
  ADD FOREIGN KEY FK_ANNEE_SALARIE_SALARIE (IDSALARIE)
      REFERENCES SALARIE (IDSALARIE) ;


ALTER TABLE ANNEE_ENFANT 
  ADD FOREIGN KEY FK_ANNEE_ENFANT_ENFANT (IDENFANT)
      REFERENCES ENFANT (IDENFANT) ;


ALTER TABLE ANNEE_ENFANT  
  ADD FOREIGN KEY FK_ANNEE_ENFANT_CADEAU_NOEL (IDCADEAU)
      REFERENCES CADEAU_NOEL (IDCADEAU) ;



