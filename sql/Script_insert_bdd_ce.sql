INSERT INTO SITE VALUES 
(1, 'Baugerie'),
(2, 'St Bernadette'),
(3, 'Pole ARTS'),
(4, 'Sainte Madeleine'),
(5, 'Route Clisson'),
(6, 'ICAM');

INSERT INTO CADEAU_NOEL VALUES
(1, 'Panier garni'),
(2, 'Jouet'),
(3, 'Chèque Kadéos');


INSERT INTO EMAIL VALUES
(1,'admin@la-joliverie.com'),
(2,'ncontant@la-joliverie.com'),
(3,'nbourgeois@la-joliverie'),
(4,'fcarbonnier@la-joliverie.com'),
(5,'fhervy@la-joliverie.com');
