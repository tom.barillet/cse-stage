<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>EmailDAO : test</title>
    </head>

    <body>

        <?php
        use modele\metier\Email;
        use modele\dao\EmailDAO;
        use modele\dao\Bdd;
        use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';

        $idEmail = '01';
        $unMail = "laubert@la-joliverie.com";
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test EmailDAO</h2>";

        // Test n°1
        echo "<h3>1- Test getAllById</h3>";
        try {
            $objet = EmailDAO::getOneById($idEmail);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°2
        echo "<h3>2- Test getAll</h3>";
        try {
            $lesObjets = EmailDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        // Test n°3
        echo "<h3>3- Test getOneByMail</h3>";
        try {
            $objet = EmailDAO::getOneById($idEmail);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°4
         echo "<h3>4- isAnExistingMail</h3>";
        try {
            $idEmail = "1";
            $ok = EmailDAO::isAnExistingMail($unMail);
            $ok = $ok && !EmailDAO::isAnExistingMail('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>

