<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SalarieDAO : test</title>
    </head>

    <body>

        <?php

        use modele\metier\Salarie;
        use modele\dao\SalarieDAO;
        use modele\dao\SiteDAO;
        use modele\metier\Site;
        use modele\dao\Bdd;
        use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';

        $idSalarie = '01';
        $unMail = "laubert@la-joliverie.com";
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test SalarieDAO</h2>";

        // Test n°1
        echo "<h3>1- Test getOneById</h3>";
        try {
            $objet = SalarieDAO::getOneById($idSalarie);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°2
        echo "<h3>2- Test getAll</h3>";
        try {
            $lesObjets = SalarieDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        // Test n°3
        echo "<h3>3- Test getOneById</h3>";
        try {
            $objet = SalarieDAO::getOneByMail($unMail);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°4
        echo "<h3>4- Test getAllPanier</h3>";
        try {
            $objet = SalarieDAO::getAllPanier();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        } 
        
             //Test n°5
        echo "<h3>5- Test getAllJouet</h3>";
        try {
            $objet = SalarieDAO::getAllJouet();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
          //Test n°6
        echo "<h3>6- Test getAllCheque</h3>";
        try {
            $objet = SalarieDAO::getAllCheque();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°4
        echo "<h3>4- Test getAllPanier</h3>";
        try {
            $objet = SalarieDAO::getAllPanier();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
             //Test n°5
        echo "<h3>5- Test getAllJouet</h3>";
        try {
            $objet = SalarieDAO::getAllJouet();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
          //Test n°6
        echo "<h3>6- Test getAllCheque</h3>";
        try {
            $objet = SalarieDAO::getAllCheque();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°7
        echo "<h3>7- Test getAllCountPanier</h3>";
        try {
            $objet = SalarieDAO::getAllCountPanier();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°8
        echo "<h3>8- Test getAllCountJouet</h3>";
        try {
            $objet = SalarieDAO::getAllCountJouet();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°9
        echo "<h3>9- Test getAllCountCheque</h3>";
        try {
            $objet = SalarieDAO::getAllCountCheque();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
               // Test n°10
        echo "<h3>10- isAnExistingId</h3>";
        try {
            $idSalarie = "01";
            $ok = SalarieDAO::isAnExistingId($idSalarie);
            $ok = $ok && !SalarieDAO::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        

            //Test n°11
        echo "<h3>11- isAnExistingMail</h3>";
        try {
            $idSalarie = "01";
            $ok = SalarieDAO::isAnExistingMail($unMail);
            $ok = $ok && !SalarieDAO::isAnExistingMail('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°3
//        echo "<h3>3- Test insert</h3>";
//        try {
////            /* @var $unSite modele\metier\Site */
////            $unSite = SiteDAO::getOneById($id);
//            $idSalarie = '3';
//            $id = '4';
//            $ok = SalarieDAO::insert($idSalarie, $id, "pfleur@la-joliverie.com", "Fleur", "Ponce", '0', "mdpfleur", '1');
//            if ($ok) {
//                echo "<h4>ooo réussite de l'insertion ooo</h4>";
//                $objetLu = SalarieDAO::getOneById($idSalarie7);
//                var_dump($objetLu);
//            } else {
//                echo "<h4>*** échec de l'insertion ***</h4>";
//            }
//        } catch (Exception $e) {
//            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
//        }
        /*
        // Test n°3-bis
        echo "<h3>3-bis insert déjà présent</h3>";
        try {
            $idRepresentation = '01';
            $objet = new Representation($idRepresentation, 'g002', '02', "2020-07-08", "13:00:00", "14:00:00");
            $ok = RepresentationDAO::insert($objet);
            if ($ok) {
                echo "<h4>*** échec du test : l'insertion ne devrait pas réussir  ***</h4>";
                $objetLu = Bdd::getOneById($idRepresentation);
                var_dump($objetLu);
            } else {
                echo "<h4>ooo réussite du test : l'insertion a logiquement échoué ooo</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>ooo réussite du test : la requête d'insertion a logiquement échoué ooo</h4>" . $e->getMessage();
        }
        
        // Test n°4
        echo "<h3>4- update</h3>";
        try {
            $objet->setDate('2020-07-08');
            $objet->setHFin('14:30:00');
            $ok = RepresentationDAO::update($idRepresentation, $objet);
            if ($ok) {
                echo "<h4>ooo réussite de la mise à jour ooo</h4>";
                $objetLu = RepresentationDAO::getOneById($idRepresentation);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de la mise à jour ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
*/
//        // Test n°5
//        echo "<h3>5- delete</h3>";
//        try {
//            $ok = RepresentationDAO::delete($idRepresentation);
////            $ok = RepresentationDAO::delete("xxx");
//            if ($ok) {
//                echo "<h4>ooo réussite de la suppression ooo</h4>";
//            } else {
//                echo "<h4>*** échec de la suppression ***</h4>";
//            }
//        } catch (Exception $e) {
//            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
//        }
//        
 
        
        Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>
