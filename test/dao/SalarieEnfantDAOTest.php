<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SalarieDAO : test</title>
    </head>

    <body>

        <?php

        use modele\metier\Salarie;
        use modele\dao\SalarieDAO;
        use modele\dao\SiteDAO;
        use modele\metier\Site;
        use modele\dao\SalarieEnfantDAO;
        use modele\dao\Bdd;
        use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';

        $idSalarie = '01';
        $unMail = "laubert@la-joliverie.com";
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test SalarieDAO</h2>";
        
             //Test n°1
        echo "<h3>1- Test getAllJouet</h3>";
        try {
            $objet = SalarieEnfantDAO::getAllJouet();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
          //Test n°2
        echo "<h3>2- Test getAllCheque</h3>";
        try {
            $objet = SalarieEnfantDAO::getAllCheque();
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
