<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Enfant Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Enfant;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Enfant</h2>";
        
        $objet =  $unEnfant = new Enfant("01", "Ketchum", "Sacha", "2018-09-05");
        var_dump($objet);
        ?>
    </body>
</html>