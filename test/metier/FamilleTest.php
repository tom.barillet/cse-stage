<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test Famille</title>
    </head>
    <body>
        <?php
        use modele\metier\Salarie;
        use modele\metier\Enfant;
        use modele\metier\Famille;
        use modele\metier\Site;
        
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Famille</h2>";
        
        $id = ('01');
        $unSite = new Site("01", "Baugerie");
        $email = ("chen@la-joliverie.com");
        $nom = ("Chen");
        $prenom = ("Professeur");
        $estOgec = ('1');
        $mdp = ("mdpchen");
        $estAdmin = ('0');

        
        $unSite = new Site("01", "Baugerie");
        $unSalarie = new Salarie("1",$unSite, "chen@la-joliverie.com", "Chen", "Professeur", '1', "mdpchen", '0' );
        $unEnfant = new Enfant("01", "Ketchum", "Sacha", "2018-09-05");
        
        $objet = new Famille($unSalarie, $unEnfant);
        var_dump($objet);
        ?>
    </body>
</html>

