<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>SalarieEnfant Test</title>
    </head>
    <body>
        <?php
        use modele\metier\SalarieEnfant;
        use modele\metier\Site;
        
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier SalarieEnfant</h2>";
        
        $id = ('01');
        $unSite = new Site("01", "Baugerie");
        $email = ("chen@la-joliverie.com");
        $nom = ("Chen");
        $prenom = ("Professeur");
        $estOgec = ('1');
        $mdp = ("mdpchen");
        $estAdmin = ('0');
        $idEnfant = ('01');
        $nomEnfant = ("Ketchum");
        $prenomEnfant = ("Sacha");
        $dateNaissance = ("2018-09-05");
        
        $objet = new SalarieEnfant($id, $unSite, $email, $nom, $prenom, $estOgec, $mdp, $estAdmin, $idEnfant, $nomEnfant, $prenomEnfant, $dateNaissance);
        var_dump($objet);
        ?>
    </body>
</html>

