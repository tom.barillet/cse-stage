<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Salarie Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Salarie;
        use modele\metier\Site;
        
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Salarie</h2>";
        
        $id = ('01');
        $unSite = new Site("01", "Baugerie");
        $email = ("chen@la-joliverie.com");
        $nom = ("Chen");
        $prenom = ("Professeur");
        $estOgec = ('1');
        $mdp = ("mdpchen");
        $estAdmin = ('0');
        
        $objet = new Salarie($id, $unSite, $email, $nom, $prenom, $estOgec, $mdp, $estAdmin);
        var_dump($objet);
        ?>
    </body>
</html>

