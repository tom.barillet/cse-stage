<?php

namespace vue\accueil;
use vue\VueGenerique;

/**
 * Description Page d'accueil du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueAccueil extends VueGenerique {

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <table width='95%'>
            <tr>
                <td class='texteAccueil'> 
                </td>
            </tr>
            <tr>
                <td >
                </td>
            </tr>
            <tr>
                <td >
                </td>
            </tr>
            <tr>
                <td class='texteAccueil'>
                    Bienvenue sur le site du comité d'entreprise de la Joliverie, ici, vous pourrez réclamer vos cadeaux à l'aide des différents formulaires.

                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td class='texteAccueil'>
                    Elle offre les services suivants :
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td class='texteAccueil'>
                    <ul>
                        <li>La gestion et la demande des cadeaux de Noël.
                            <br>
                        <li>La gestion et la demande des chèques vacances.

                    </ul>
                </td>
            </tr>
            <tr>
                <td class='texteAccueil2'>
                    Afin d'utiliser les différents menus à votre disposition, connectez ou inscrivez-vous avec le bouton situé en haut à gauche
                </td>
            </tr>
        </table>

<br/> <br/> <br/> <br/> <br/> <br/>


<img src="vue/images/noel.jpg" alt="image1" style="display:inline-block;" class="arrondie2"/> 
<img src="vue/images/noel2.jpg" alt="image2" style="display:inline-block;" class="arrondie2"/> 
<img src="vue/images/spectacle.jpg" alt="image3" style="display:inline-block;" class="arrondie2"/>
<img src="vue/images/spectacle1.jpg" alt="image4" style="display:inline-block;" class="arrondie2"/>



        <?php
        include $this->getPied();
    }

}
