<?php

namespace vue\authentification;

use vue\VueGenerique;

/**
 * Description Page d'authentification d'un utilisateur
 */
class VueSAuthentifier extends VueGenerique {

    /** @var string login à afficher */
    private $adressemail;
    /** @var string mot de passe */
    private $motdepasse;

    /** @var string à afficher en tête du formulaire */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=authentification&action=authentifier">
            <br>
            <table width="30%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="2"><strong><?= $this->message ?></strong></td>
                </tr>

                <tr class="ligneTabNonQuad">
                    <td> Adresse Mail*: </td>
                    <td><input type="text" value="<?= $this->adressemail ?>" name="adressemail" size ="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Mot de passe*: </td>
                    <td><input type="password" value="<?= $this->motdepasse ?>" name="motdepasse" size="25" maxlength="30"></td>
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
           
            <table>
                <tr>
                    <td align='center'> <a href="index.php">Retour</a></td>
                </tr>
                <tr>
                    <td align="center">Si vous n'avez pas encore créé de compte, <a href="index.php?controleur=authentification&action=inscription">inscrivez-vous</a></td>
            </tr>
            <tr>
            <td align="center">Si vous rencontrez des problèmes, contactez un administrateur, <a href="index.php?controleur=authentification&action=defautNonConnecte">ici</a> </td>
            </tr>
            </table>
        </form>
        <?php
        include $this->getPied();
    }

    function getAdressemail(): string {
        return $this->adressemail;
    }

    function getMotdepasse(): string {
        return $this->motdepasse;
    }

    function getMessage(): string {
        return $this->message;
    }

    function setAdressemail(string $adressemail): void {
        $this->adressemail = $adressemail;
    }

    function setMotdepasse(string $motdepasse): void {
        $this->motdepasse = $motdepasse;
    }

    function setMessage(string $message): void {
        $this->message = $message;
    }





}
