<?php

namespace vue\contact;
use vue\VueGenerique;
use modele\metier\Salarie;


/**
 * Description Page de contact du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueContact extends VueGenerique {

    private $lesSalaries;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <span id="texteNiveau2" class="texteNiveau2">
        Si vous avez des questions ou des problèmes, veuillez contacter l'une des personnes suivantes : 
        </span>
        <br><br>
        <table width='22%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'>
            <tr class='enTeteTabNonQuad'>
                <td colspan='7'><strong>Salariés</strong></td>
            </tr>
        <?php 
        foreach($this->lesSalaries as $unSalarie){
                ?>
                <tr class='ligneTabNonQuad'> 
                    <td width='13%'><?= $unSalarie->getPrenom() ?></td>
                    <td width='13%'><?= $unSalarie->getNom() ?></td>
                    <td width='40%'><a href="mailto:<?= $unSalarie->getEmail() ?>"><?= $unSalarie->getEmail() ?></td>
                </tr>    
            <?php }
        ?>    
        </table>
            
        <?php
        include $this->getPied();
    }
    public function setLesSalaries($lesSalaries) {
        $this->lesSalaries = $lesSalaries;
    }

}
