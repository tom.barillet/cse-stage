<?php 
namespace vue\csv;

class VueGenererJouetCSV {

}
/** @var array liste des Salaries qui ont choisi le jouet à afficher  */
    $lesSalariesJouet=$_SESSION['unSalarieJouet'];
    
/** @var int nombre des Salaries qui ont choisi le jouet  */
    $lesSalariesCountJouet=$_SESSION['unSalarieCountJouet'];

//Fetch all of the rows from our MySQL table
$rows = $lesSalariesJouet;
//var_dump($rows);
//Get the column names.
$columnNames = array();
if(!empty($rows)){
    //We only need to loop through the first row of our result
    //in order to collate the column names.
    $firstRow = $rows[0];
    foreach($firstRow as $colName => $val){
        $columnNames[] = $colName;
    }
}

//Setup the filename that our CSV will have when it is downloaded.
$fileName = 'commande_jouet.csv';

//Set the Content-Type and Content-Disposition headers to force the download.
header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="' . $fileName . '"');

//Open up a file pointer
$fp = fopen('php://output', 'w');

//Start off by writing the column names to the file.
fputcsv($fp, $columnNames);

//Then, loop through the rows and write them to the CSV file.
foreach ($rows as $row) {
    fputcsv($fp, $row);
}

//Close the file pointer.
fclose($fp);
