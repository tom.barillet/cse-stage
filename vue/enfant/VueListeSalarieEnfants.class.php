<?php

namespace vue\enfant;
use vue\VueGenerique;
use modele\metier\Salarie;

/**
 * Description Page du formulaire du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueListeSalarieEnfants extends VueGenerique {

    private $lesEnfants;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        
            if (empty($this->lesEnfants)){
                
                ?>
              <form method="POST" action="index.php?controleur=salarie&action=validerChoixParent">

                <table width="23%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong>Modification des enfants</strong></td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <td><strong>Ce salarié n'a rentré aucun enfant sur le site.</strong></td>
                </tr>
              
            </table>
              
            <?php
            }
            else {
                ?>
                 <table width="30%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong>Modification des enfants</strong></td>
                </tr>
                <?php
            // Pour chaque enfant
            foreach ($this->lesEnfants as $unEnfant) {
                ?>
            <tr class="ligneTabNonQuad">
                <td colspan='2'><strong>Modifier l'enfant :</strong>
                <td colspan='2'><a href = 'index.php?controleur=enfant&action=modifierEnfant&idEnfant=<?= $unEnfant->getId() ?>'><?= $unEnfant->getPrenom()." ".$unEnfant->getNom()?></a></td>
            </tr>
            
            <?php } }
        ?> 
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center"><a href='index.php?controleur=salaries&action=liste'>Retour</a>
                    </td>
                </tr>
            </table>
             
        </table>
        <br>
        </form>
        <?php
        include $this->getPied();
    }
    function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }
    public function setEnfants(Array $lesEnfants) {
        $this->lesEnfants = $lesEnfants;
    }
            
       
    }



 
        
        