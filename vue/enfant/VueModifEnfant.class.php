<?php

namespace vue\enfant;

use vue\VueGenerique;
use modele\metier\Salarie;
use modele\dao\SiteDAO;
use modele\metier\Enfant;
use modele\dao\EnfantDAO;

/**
 * Description Page de saisie d'un enfant 
 * @author Barillet/Menadier
 * @version 2021
 */
class VueModifEnfant extends VueGenerique {
    /** @var Salarie salarie à modifier */
    private $unEnfant;
    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;
    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;
    /** @var string à afficher en tête du tableau */
    private $message;
    // constructeur
    public function __construct() {
        parent::__construct();
    }
    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=salaries&action=<?=$this->actionAEnvoyer ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>                
               
                <tr class="autreLigne">
                        <td><input type="hidden" value="<?= $this->unEnfant->getId();
                            ?>" name="idEnfant"></td><td></td>
                    </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nom*: </td>
                    <td><input type="text" value="<?= $this->unEnfant->getNom(); ?>" name="nom" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Prénom*: </td>
                    <td><input type="text" value="<?= $this->unEnfant->getPrenom(); ?>" name="prenom" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Date de Naissance*: </td>
                    <td><input type="date" value="<?= $this->unEnfant->getDateNaissance(); ?>" name="dateNaissance" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
              </form>  
            <br> <br>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center"><a href='index.php?controleur=salaries&action=liste'>Retour</a>
                    </td>
                </tr>
                <tr>
                    <td align="center">Supprimer l'enfant <a href="index.php?controleur=salaries&action=supprimerEnfant&id=<?= $this->unEnfant->getId() ?>"><?= $this->unEnfant->getNom()." ".$this->unEnfant->getPrenom(); ?></a> </td>
                </tr>
            </table>
            <br>
           
        <?php
        include $this->getPied();
    }

    public function getUnEnfant(): Enfant {
        return $this->unEnfant;
    }

    public function getActionRecue() {
        return $this->actionRecue;
    }

    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setUnEnfant(Enfant $unEnfant) {
        $this->unEnfant = $unEnfant;
    }

    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

}