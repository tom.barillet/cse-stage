<?php

namespace vue\enfant;

use vue\VueGenerique;
use modele\metier\Enfant;

/**
 * Description Page de suppression d'un groupe donné
 * @author Tom
 * @version 2020
 */
class VueSupprimerEnfant extends VueGenerique {

    /** @var Groupe groupe à modifier */
    private $unEnfant;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer l'enfant
        <?= $this->unEnfant->getPrenom() ?> <?= $this->unEnfant->getNom() ?> ?
            <h3><br>
                <a href="index.php?controleur=salaries&action=validerSupprimerEnfant&id=<?=$this->unEnfant->getId() ?>">Oui</a>
                <a href="index.php?controleur=salaries">Non</a></h3></center>
        <?php
        include $this->getPied();
    }
    // ACCESSEURS ET MUTATEURS
    public function getUnEnfant(): Enfant {
        return $this->unEnfant;
    }
    public function getActionRecue() {
        return $this->actionRecue;
    }
    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }
    public function getMessage() {
        return $this->message;
    }
    public function setUnEnfant(Enfant $unEnfant) {
        $this->unEnfant = $unEnfant;
    }
    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }
    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
}