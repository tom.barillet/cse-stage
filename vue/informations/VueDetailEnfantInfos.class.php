<?php

namespace vue\informations;
use vue\VueGenerique;
use modele\metier\Cadeau;
use modele\metier\Enfant;
use includes\fonctionsDateTimes;

/**
 * Description Page du formulaire du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueDetailEnfantInfos extends VueGenerique {

    
    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $unEnfant;
    private $unCadeau;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
       
       <table width="20%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">
                 <tr class="enTeteTabNonQuad">
                     <td colspan="5"><strong>Informations de l'enfant</strong></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td><strong>Nom/prénom :</strong></td> 
                    <td><?= $this->unEnfant->getNom()?> <?= $this->unEnfant->getPrenom()?></td>              
                </tr>
                <tr class="ligneTabNonQuad">
                    <td><strong>Date de naissance :</strong></td> 
                    <td><?= dateMysql2Fr($this->unEnfant->getDateNaissance())?></td>              
                </tr>
                <tr class="ligneTabNonQuad">
                    <td><strong>Cadeau choisi :</strong></td> 
                    <td><?= $this->unCadeau->getLibelleCadeau()?></td>              
                </tr>
        </table>
        <br>      
          <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center"><a href='index.php?controleur=infos&action=liste'>Retour</a>
                    </td>
                </tr>
            </table>
        
        <?php
        include $this->getPied();
    }
    
    function setUnEnfant(Enfant $unEnfant) {
        $this->unEnfant = $unEnfant;
    }
   function setUnCadeau(Cadeau $unCadeau) {
        $this->unCadeau = $unCadeau;
    }
            
       
    }



 
        
        