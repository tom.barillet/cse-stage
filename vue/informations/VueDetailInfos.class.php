<?php

namespace vue\informations;

use vue\VueGenerique;
use modele\metier\Salarie;
use modele\metier\Enfant;
/**
 * Description Page de consultation d'un salarié donné
 * @author Barillet/Menadier
 * @version 2021
 */
class VueDetailInfos extends VueGenerique {

    /** @var Salarie identificateur du Salarie à afficher */
    private $unSalarie;
    private $lesEnfants;
    public function __construct() {
        parent::__construct();
    }
    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
            <?php
            // Pour chaque Enfant
            if(empty($this->lesEnfants)){
                ?>
                <table width='21%' cellspacing='0' cellpadding='0'  class='tabNonQuadrille'> 

                <tr class="ligneTabNonQuad">
                    <td colspan='3'><strong>Vous n'avez pas d'enfants rentrés sur le site.</strong>
                </tr>
                </table>
            <?php
            }
            foreach ($this->lesEnfants as $unEnfant) {
                ?>
            <table width='20%' cellspacing='0' cellpadding='0'  class='tabNonQuadrille'> 

            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Nom/prénom :</strong>
                <td colspan='3'><strong><a href="index.php?controleur=infos&action=detailEnfant&idEnfant=<?= $unEnfant->getId() ?>"><?= $unEnfant->getNom()?> <?= $unEnfant->getPrenom()?></a> </strong></td>
            </tr>
            <?php }
        ?> 
        </table>
        <br>
        <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center"><a href='index.php?controleur=infos&action=liste'>Retour</a>
                    </td>
                </tr>
            </table>
       
        <?php
        include $this->getPied();
    }

    function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }
    public function setEnfants(Array $lesEnfants) {
        $this->lesEnfants = $lesEnfants;
    }

}

