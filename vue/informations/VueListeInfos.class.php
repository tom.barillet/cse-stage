<?php
/*
 * Description Page de consultation de la liste des informations
 * -> affiche Uun tableau constitué d'une ligne d'entête et d'une ligne d'informations
 * @author Barillet/Menadier
 * @version 2021
 */
namespace vue\informations;

use vue\VueGenerique;
use modele\metier\Salarie;
use modele\metier\Enfant;

class VueListeInfos extends VueGenerique {
    
    /** @var array liste des informations à afficher avec leur nombre d'atttributions */
    private $unSalarie;
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
    <table width="50%" cellspacing="0" cellpadding="1" class="tabQuadrille">

        <tr class="enTeteTabQuad">
            <td width="20%">Nom</td>
            <td width="20%">Prénom</td> 
            <td width="30%">Mail</td> 
            <td width="20%">Site</td>
            <td width="35%">Enfant(s)</td>
        </tr>
        <?php
        // Pour chaque salarié qui est connecté afficher ses informations
        ?>                        
        <tr class="ligneTabNonQuad">
            <td ><?=$this->unSalarie->getNom()?></td>
            <td><?=$this->unSalarie->getPrenom()?></td> 
            <td><?=$this->unSalarie->getEmail()?></td> 
            <td><?=$this->unSalarie->getIdSite()->getNom()?></td>
            <td ><a href="index.php?controleur=infos&action=detail&id=<?= $this->unSalarie->getId() ?>"> Mes enfants</a></td>
        </tr>                   
        <?php
        
        ?>
    </table>
        <br> <br> <br>
            Ajouter des <a href="index.php?controleur=enfant&action=defaut">enfants.</a> 
    
    <br>


        <?php
            include $this->getPied();

       }
      public function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }

}