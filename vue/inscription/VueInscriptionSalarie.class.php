<?php

namespace vue\inscription;

use vue\VueGenerique;
use modele\metier\Salarie;
use modele\metier\Site;
use modele\dao\SalarieDAO;
use modele\dao\SiteDAO;

/**
 * Description Page d'authentification d'un utilisateur
 */
class VueInscriptionSalarie extends VueGenerique {

    
    /** @var Representation representation à afficher */
    private $unSalarie;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;
    
    /** @var array Site tous les sites */
    private $lesSites;
    private $unSite;
 

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=authentification&action=<?= $this->actionAEnvoyer ?>">            
            <br>
            <table width="30%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="2"><strong>Inscription</strong></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nom*: </td>
                    <td><input type="text" value="<?= $this->unSalarie->getNom(); ?>" name="nom" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Prénom*: </td>
                    <td><input type="text" value="<?= $this->unSalarie->getPrenom(); ?>" name="prenom" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Adresse Mail*: </td>
                    <td><input type="text" placeholder="pnom@la-joliverie.com" value="<?= $this->unSalarie->getEmail(); ?>" name="adressemail" size ="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Mot de passe*: </td>
                    <td><input type="password" value="<?= $this->unSalarie->getMdp(); ?>" name="motdepasse" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Site Principal* :</td>
                    <td>
                        <select name="site" id="site">
                            <option selected disabled hidden value="defaut" > Votre site :</option>
                            <?php
                            foreach($this->lesSites as $leSite) {
                                echo '<option value="'.$leSite->getId().'">'.$leSite->getNom().'</option>';
                            }
                            ?>               
                    </select>
                    </td>
                </tr>                         
                <tr class="ligneTabNonQuad"> 
                    <td> Type de salarié* :  </td>
                    <td></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td><input type="radio" id="OGEC" name="typeSalarie" value=1> OGEC</td>
                    <td><input type="radio" id="Professeur" name="typeSalarie" value=0> Professeur</td>
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <br> <br>
            <a href="index.php">Retour</a>
             
        </form>
        <?php
        include $this->getPied();
    }

    function getAdressemail(): string {
        return $this->adressemail;
    }

    function getMotdepasse(): string {
        return $this->motdepasse;
    }

    function getMessage(): string {
        return $this->message;
    }

    function setAdressemail(string $adressemail): void {
        $this->adressemail = $adressemail;
    }

    function setMotdepasse(string $motdepasse): void {
        $this->motdepasse = $motdepasse;
    }

    function setMessage(string $message): void {
        $this->message = $message;
    }

    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }
    
    public function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }
    
    public function setLesSites($lesSites) {
        $this->lesSites = $lesSites;
    }
    
    public function setUnSite($unSite) {
        $this->unSite = $unSite;
    }


}
