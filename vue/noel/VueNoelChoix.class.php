<?php

namespace vue\Noel;
use vue\VueGenerique;
use modele\metier\Salarie;
use modele\metier\Enfant;

/**
 * Description Page du formulaire du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueNoelChoix extends VueGenerique {

    
    private $actionRecue;
    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;
    private $unEnfant;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <span id="texteNiveau2" class="texteNiveau2">
        Si votre conjoint(e) est également un(e) salarié(e) de la Joliverie, merci de ne faire qu'une demande par couple
        </span>
        <br>
            <span id="texteNiveau2" class="texteNiveau2">
                Choississez le cadeau pour <?php echo($this->unEnfant->getPrenom(). " ".$this->unEnfant->getNom())?> :
        </span>

        <br> <br> <br>
      <form method="POST" action="index.php?controleur=noel&action=<?= $this->actionAEnvoyer ?>&idEnfant=<?= $this->unEnfant->getId() ?>">

       <table width="15%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="2"><strong>Choix du cadeau</strong></td>
                </tr>
                <tr class="ligneTabNonQuad">
                   <td><input type="radio" id="cheque" name="idCadeau" value="3"> Chèque Cadeau</td> 
                </tr>
                <tr class="ligneTabNonQuad">
                    <td><input type="radio" id="jouet" name="idCadeau" value="2"> Jouet</td>                    
                </tr>
        </table>
        <br>
        <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
        <br>
        </form>       
          <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center"><a href='index.php?controleur=noel&action=liste'>Retour</a>
                    </td>
                </tr>
            </table>
        
        <?php
        include $this->getPied();
    }
    
    function setUnEnfant(Enfant $unEnfant) {
        $this->unEnfant = $unEnfant;
    }
      public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }
            
       
    }



 
        
        