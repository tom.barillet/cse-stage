<?php

namespace vue\Noel;
use vue\VueGenerique;
use modele\metier\Salarie;

/**
 * Description Page du formulaire du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueNoelConfirmation extends VueGenerique {

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <span id="texteNiveau2" class="texteNiveau2">
        Si votre conjoint(e) est également un(e) salarié(e) de la Joliverie, merci de ne faire qu'une demande par couple : 
        </span>
        <br> <br> <br>
       
                
                 <table width="15%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong>Formulaire Noël</strong></td>
                </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='2'><strong>Votre choix a bien été enregistré</strong>
            </tr>
            
           
        </table>
        <br>
        <a href='index.php?controleur=accueil'>Retour</a>
        <?php
        include $this->getPied();
    
        
    }
    }



 
        
        