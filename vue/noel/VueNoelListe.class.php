<?php

namespace vue\Noel;
use vue\VueGenerique;
use modele\metier\Salarie;


/**
 * Description Page du formulaire du site
 *
 * @author Barillet/Menadier
 * @version 2021
 */
class VueNoelListe extends VueGenerique {

    private $lesEnfants;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
      
            <?php
            if (empty($this->lesEnfants)){
                
                ?>
              <form method="POST" action="index.php?controleur=noel&action=validerChoixParent">

                <table width="15%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong>Formulaire Noël</strong></td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <td><strong>Panier garni :</strong></td>
                    <td><input type="checkbox" id="panier" name="panier" value="1"></td>
                </tr>
                </table>
        <br>
        <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
              </form>
            <?php
            }
            else {
                ?>
  <span id="texteNiveau2" class="texteNiveau2">
        Si votre conjoint(e) est également un(e) salarié(e) de la Joliverie, merci de ne faire qu'une demande par couple : 
        </span>
        <br> <br> <br>
       
                 <table width="30%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong>Formulaire Noël</strong></td>
                </tr>
                <?php
            // Pour chaque enfant
            foreach ($this->lesEnfants as $unEnfant) {
                ?>
            <tr class="ligneTabNonQuad">
                <td colspan='2'><strong>Choississez un cadeau pour :</strong>
                <td colspan='2'><a href = 'index.php?controleur=noel&action=choixEnfant&idEnfant=<?= $unEnfant->getId() ?>'><?= $unEnfant->getPrenom()." ".$unEnfant->getNom()?></a></td>
            </tr>
            
            <?php } }
        ?> 
            </table>
            
             <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center"><a href='index.php?controleur=accueil'>Retour</a>
                    </td>
                </tr>
            </table>
        
        <br>
        <?php
        include $this->getPied();
    }
    function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }
    public function setEnfants(Array $lesEnfants) {
        $this->lesEnfants = $lesEnfants;
    }
            
       
    }



 
        
        