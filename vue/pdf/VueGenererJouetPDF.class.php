<?php 
namespace vue\pdf;

use Spipu\Html2Pdf\Html2Pdf;

require ('vendor/autoload.php');


class VueGenererJouetPDF extends Html2Pdf {

}

ob_start();

?>


<style type="text/css">
<!--
table
{
    width:  100%;
    border:none;
    border-collapse: collapse;
}
th
{
    text-align: center;
    border: solid 1px #eee;
    background: #f8f8f8;
}
td
{
    text-align: center;
  
}
.dataTable td{
padding:10px 5px;
background-color:#efefef;
}
.dataTable th{
padding:10px 5px;
}
-->
</style>

<?php

    /** @var array liste des Salaries qui ont choisi le jouet à afficher  */
    $lesSalariesJouet=$_SESSION['unSalarieJouet'];
    
    /** @var int nombre des Salaries qui ont choisi le jouet  */
    $lesSalariesCountJouet=$_SESSION['unSalarieCountJouet'];

?>

<page style="font-size: 12pt" >
    <table cellspacing="0" style="width: 100%; text-align: center; font-size: 14px">
        <tr>
            <td style="width: 25%; color: #444444;"></td>
            <td style="width: 25%; color: #444444;">
                <img width="150%;" src="vue/images/EnteteCSE.png" alt="Logo"><br><br><br><br>
            </td>
        </tr>
</table>
    <br>
    <br>
    <table cellspacing="0" style="width: 100%; text-align: left;font-size: 10pt">
        <tr>
            <td style="width:50%;"></td>
            <td style="width:50%; ">A la Joliverie, le<?php echo date(' d/m/Y');?></td><br><br><br><br>
        </tr>
    </table>
    <br>
    <i>
        <br>
        <b><u>Objet </u>: &laquo; Commande de Noël pour les jouets &raquo;</b><br><br>
    </i>

    
     <table class="dataTable" cellspacing="0" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 8pt;">
        <tr>
            <th style="width: 14%">Nom</th>
            <th style="width: 14%">Prénom</th>
            <th style="width: 22%">Adresse Mail</th>
            <th style="width: 14%">Nom enfant</th>
            <th style="width: 14%">Prénom enfant</th>
            <th style="width: 10%">Date de Naissance</th>
            <th style="width: 12%">Emargement</th>
        </tr>

       <?php
         //Debogage
         //var_dump($lesSalariesJouet);
         //var_dump($_SESSION);
        
         //Pour chaque salariés, afficher son nom, prenom, adresse mail, nom de l'enfant, prenom de l'enfant et date de naissance de l'enfant si il a commandé un panier garnis
         foreach ($lesSalariesJouet as $unSalarieJouet) {
         //var_dump($unSalarieJouet->getNom());   
                ?>
                <tr >
                    <td style="width: 14%"><?php echo $unSalarieJouet->getNom();?></td>
                    <td style="width: 14%"><?php echo $unSalarieJouet->getPrenom();?></td>
                    <td style="width: 22%"><?php echo $unSalarieJouet->getEmail();?></td>
                    <td style="width: 14%"><?php echo $unSalarieJouet->getNomEnfant();?></td>
                    <td style="width: 10%"><?php echo $unSalarieJouet->getPrenomEnfant();?></td>
                    <td style="width: 10%"><?php echo $unSalarieJouet->getDateNaissance(); ?></td>
                    <td style="width: 12%"></td>
                </tr>
            <?php
            }
            ?>
    </table>
    
        
    <br><br>
    Tableau récapitulatif du nombre d'articles commandés : <br />
    <br>
    
     <table class="dataTable" cellspacing="0" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 10pt;">
        <tr>
            <th style="width: 20%">Jouets</th>
        </tr>

        <tr>
            <td style="width: 20%"><?php echo $lesSalariesCountJouet;?></td>
        </tr>
    </table>
    <page_footer>
    <p style="text-align: center"> [[page_cu]]/[[page_nb]] </p>
  </page_footer>
</page>
<br /><br /><br /><br /><br />

<?php 
    $content = ob_get_clean();
    require_once( 'vendor/autoload.php');
    try
    {
        $html2pdf = new HTML2PDF("P", "A4", "fr");
       
        $html2pdf->setDefaultFont("Arial");
        $html2pdf->writeHTML($content);
        $html2pdf->Output("noel.pdf");
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }



// ACCESSEUR et MUTATEURS
    
    function setUnSalarieJouet(Array $lesSalariesJouet) {
        $this->lesSalariesJouet = $lesSalariesJouet;
    }

    
?>

