<?php 
namespace vue\pdf;

use Spipu\Html2Pdf\Html2Pdf;

require ('vendor/autoload.php');


class VueGenererPanierPDF extends Html2Pdf {

}

ob_start();

?>


<style type="text/css">
<!--
table
{
    width:  100%;
    border:none;
    border-collapse: collapse;
}
th
{
    text-align: center;
    border: solid 1px #eee;
    background: #f8f8f8;
}
td
{
    text-align: center;
  
}
.dataTable td{
padding:10px 5px;
background-color:#efefef;
}
.dataTable th{
padding:10px 5px;
}
-->
</style>

<?php
/** @var array liste des Salaries qui ont choisi le panier à afficher  */
    $lesSalariesPanier=$_SESSION['unSalariePanier'];
        
    /** @var int nombre des Salaries qui ont choisi le panier  */
    $lesSalariesCountPanier=$_SESSION['unSalarieCountPanier'];
    
?>

<page style="font-size: 12pt" >

    <table cellspacing="0" style="width: 100%; text-align: center; font-size: 14px">
        <tr>
            <td style="width: 25%; color: #444444;"></td>
            <td style="width: 25%; color: #444444;">
                <img width="150%;" src="vue/images/EnteteCSE.png" alt="Logo"><br><br><br><br>
            </td>
        </tr>
</table>
    <br>
    <br>
    <table cellspacing="0" style="width: 100%; text-align: left;font-size: 10pt">
        <tr>
            <td style="width:50%;"></td>
            <td style="width:50%; ">A la Joliverie, le<?php echo date(' d/m/Y');?></td><br><br><br><br>
        </tr>
    </table>
    <br>
    <i>
        <br>
        <b><u>Objet </u>: &laquo; Commande de Noël des paniers garnis &raquo;</b><br><br>
    </i>

    <table class="dataTable" cellspacing="0" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 10pt;">
        <tr>
            <th style="width: 25%">Nom</th>
            <th style="width: 25%">Prénom</th>
            <th style="witdh: 25%">Adresse Mail </th>
            <th style="witdh: 25%">Emargement </th>
        </tr>
         <?php
         //Debogage
         //var_dump($lesSalariesPanier);
         //var_dump($_SESSION);
        
         //Pour chaque salariés, afficher son nom, prenom, adresse mail si il a commandé un panier garnis
         foreach ($lesSalariesPanier as $unSalariePanier) {
         //var_dump($unSalariePanier->getNom());   
                ?>
                <tr >
                    <td style="width: 25%"><?php echo $unSalariePanier->getNom();?></td>
                    <td style="width: 25%"><?php echo $unSalariePanier->getPrenom();?></td>
                    <td style="width: 25%"><?php echo $unSalariePanier->getEmail() ;?></td>
                    <td style="width: 25%"></td>
                </tr>
            <?php
            }
            ?>              
    </table> 

    <br><br>
    Tableau récapitulatif du nombre d'articles commandés : <br />
    <br>
    
     <table class="dataTable" cellspacing="0" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 10pt;">
        <tr>
            <th style="width: 20%">Total</th>
        </tr>

        <tr>
            <td style="width: 20%"><?php echo $lesSalariesCountPanier;?></td>
        </tr>
    </table>

<page_footer>
    <p style="text-align: center"> [[page_cu]]/[[page_nb]] </p>
  </page_footer>

    
</page>
<br /><br /><br /><br /><br />

<?php 
    $content = ob_get_clean();
    require_once( 'vendor/autoload.php');
    try
    {
        $html2pdf = new HTML2PDF("P", "A4", "fr");
       
        $html2pdf->setDefaultFont("Arial");
        $html2pdf->writeHTML($content);
        $html2pdf->Output("noel.pdf");
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }



// ACCESSEUR et MUTATEURS
    function setUnSalariePanier(Array $lesSalariesPaniers) {
        $this->lesSalariesPaniers = $lesSalariesPaniers;
    }
    
?>

