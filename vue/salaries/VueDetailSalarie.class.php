<?php

namespace vue\salaries;

use vue\VueGenerique;
use modele\metier\Salarie;
use modele\metier\Enfant;
/**
 * Description Page de consultation d'un salarié donné
 * @author Barillet/Menadier
 * @version 2021
 */
class VueDetailSalarie extends VueGenerique {

    /** @var Salarie identificateur du Salarie à afficher */
    private $unSalarie;
    private $lesEnfants;
    public function __construct() {
        parent::__construct();
    }
    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width='22%' cellspacing='0' cellpadding='0'  class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='4'> <strong>Salarié</strong> </td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Prénom :</strong></td>
                <td colspan='3'><strong><?= $this->unSalarie->getPrenom() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Nom :</strong></td>
                <td colspan='3'><strong><?= $this->unSalarie->getNom() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Adresse mail :</strong></td>
                <td colspan='3'><strong><?= $this->unSalarie->getEmail() ?></strong></td>
            </tr>
            
            <tr class="ligneTabNonQuad">
                  <?php
                    if( $this->unSalarie->getAdmin() == 1){
                    ?>
                    <td colspan='3'><strong>Est administrateur :</strong></td>
                    <td colspan='3'><strong>Oui</strong></td>
                
                         <?php   
                    }
                    else {
                        ?>
                    <td colspan='3'><strong>Est administrateur :</strong></td>
                    <td colspan='3'><strong>Non</strong></td>
                    
                    <?php   
     
                    }
                
                ?>
               
            </tr>
            <tr class="ligneTabNonQuad">
                
                <?php
                    if( $this->unSalarie->getOgec() == 1){
                    ?>
                    <td colspan='3'><strong>OGEC/Professeur :</strong></td>
                    <td colspan='3'><strong>OGEC</strong></td>
                
                         <?php   
                    }
                    else {
                        ?>
                    <td colspan='3'><strong>OGEC/Professeur :</strong></td>
                    <td colspan='3'><strong>Professeur</strong></td>
                    
                    <?php   
     
                    }
                
                ?>
                
            </tr>
        </table>
             <?php
            // Pour chaque enfant
            foreach ($this->lesEnfants as $unEnfant) {
                ?>
              <table width='22%' cellspacing='0' cellpadding='0'  class='tabNonQuadrille'> 

            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Nom/prénom :</strong>
                <td colspan='3'><strong><?= $unEnfant->getNom()?> <?= $unEnfant->getPrenom()?></a> </strong></td>
            </tr>
            <?php }
        ?> 
        </table>
        <br>
        <a href='index.php?controleur=salaries&action=liste'>Retour</a>
        <?php
        include $this->getPied();
    }

   
    function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }
    public function setEnfants(Array $lesEnfants) {
        $this->lesEnfants = $lesEnfants;
    }

}
