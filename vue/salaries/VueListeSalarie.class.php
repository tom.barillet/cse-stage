<?php

namespace vue\salaries;

use vue\VueGenerique;

/**
 * Description Page de consultation des salariés
 * @author Tom
 * @version 2020
 */
class VueListeSalarie extends VueGenerique {

    /** @var array liste des Salaries à afficher  */
    private $lesSalaries;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Implémentation de la méthode générant le code HTML de la page concernée
     */
    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width='40%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'>
            <tr class='enTeteTabNonQuad'>
                <td colspan='7'><strong>Salariés</strong></td>
            </tr>
            <?php
            // Pour chaque Salarié
            foreach ($this->lesSalaries as $unSalarie) {
                ?>
                <tr class='ligneTabNonQuad'> 
                    <td width='5%'><?= $unSalarie->getId() ?></td>
                    <td width='15%'><?= $unSalarie->getPrenom() ?></td>
                    <td width='15%'><?= $unSalarie->getNom() ?></td>
                    <td width='15%' align='center'>         
                        <a href="index.php?controleur=salaries&action=modifier&id=<?= $unSalarie->getId() ?>">
                            Modifier       
                        </a></td>
                    <td width='15%' align='center'> 
                        <a href="index.php?controleur=salaries&action=supprimer&id=<?= $unSalarie->getId() ?>">
                            Supprimer               
                        </a></td>
                    <td width='15%' align='center'> 
                        <a href="index.php?controleur=salaries&action=detail&id=<?= $unSalarie->getId() ?>">
                            Voir Details
                        </a></td>
                    <td width='20%' align='center'> 
                        <a href="index.php?controleur=enfant&action=modifierEnfantsListe&id=<?= $unSalarie->getId() ?>">
                            Modifier enfants
                        </a></td>
            <?php }
        ?>    
        </table><br>    
        <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="center">Récapitulatif de la commande des <a href="index.php?controleur=salaries&action=genererPanier" onclick="window.open(this.href);return false" >paniers garnis.</a> <br> <br></a>
                    </td>
                </tr>
                 <tr>
                    <td align="center">Récapitulatif de la commande des <a href="index.php?controleur=salaries&action=genererJouet" onclick="window.open(this.href);return false"> jouets.</a> <br> <br>

                    </td>
                </tr>
                <tr>
                    <td align="center">Récapitulatif de la commande des <a href="index.php?controleur=salaries&action=genererJouetCSV" onclick="window.open(this.href);return false"> jouets.</a> (fichier excel) <br> <br>

                    </td>
                </tr>
                 <tr>
                    <td align="center">Récapitulatif de la commande des <a href="index.php?controleur=salaries&action=genererCheque" onclick="window.open(this.href);return false">chèques cadeaux.</a> <br> <br>

                    </td>
                </tr>
            </table>  
         <br> <br> <br>
         
            
    
    <br>

        <?php
        include $this->getPied();
    }

    // ACCESSEUR et MUTATEURS
    public function setSalarie(Array $lesSalaries) {
        $this->lesSalaries = $lesSalaries;
    }

}
