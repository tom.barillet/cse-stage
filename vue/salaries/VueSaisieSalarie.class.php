<?php

namespace vue\salaries;

use vue\VueGenerique;
use modele\metier\Salarie;
use modele\metier\Contact;
use modele\dao\SiteDAO;

/**
 * Description Page de saisie/modification d'un salarié donné
 * @author Barillet/Menadier
 * @version 2021
 */
class VueSaisieSalarie extends VueGenerique {
    /** @var Salarie salarie à modifier */
    private $unSalarie;
    private $unContact;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;
    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;
    /** @var string à afficher en tête du tableau */
    private $message;
    // constructeur
    public function __construct() {
        parent::__construct();
    }
    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=salaries&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>
                <?php
                // En cas de création, l'id est accessible sinon l'id est dans un champ
                // caché
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->getUnSalarie()->getId() 
                            ?>" name="id" size ="2"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr class="autreLigne">
                        <td><input type="hidden" value="<?= $this->getUnSalarie()->getId() 
                            ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>

                  
                <tr class="ligneTabNonQuad">
                    <td></td>
                    <td></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nom*: </td>
                    <td><input type="text" value="<?= $this->unSalarie->getNom(); ?>" name="nom" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Prénom*: </td>
                    <td><input type="text" value="<?= $this->unSalarie->getPrenom(); ?>" name="prenom" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Adresse Mail*: </td>
                    <td><input type="text" value="<?= $this->unSalarie->getEmail(); ?>" name="adressemail" size ="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Mot de passe*: </td>
                    <td><input type="password" value="<?= $this->unSalarie->getMdp(); ?>" name="motdepasse" size="25" maxlength="30"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Site Principal* :</td>
                    <td>
                        <?php 

                        $unSite = SiteDAO::getOneById($this->unSalarie->getIdSite()->getId());

                            
                            ?>
                        
                        <select name="site" id="site">
                            <option selected value=<?=$unSite->getId() ?> > <?= $unSite->getNom() ?> </option>
                            <?php
                            foreach($this->lesSites as $leSite) {
                                echo '<option value="'.$leSite->getId().'">'.$leSite->getNom().'</option>';
                            }
                            ?>               
                    </select>
                    </td>
                </tr>                         
                <tr class="ligneTabNonQuad"> 
                    <td> Type de salarié* :  </td>
                    <td></td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <?php 
                            
                        if($this->unSalarie->getOgec() === "1"){
                            ?>
                    <td><input type="radio" id="OGEC" name="typeSalarie" value="1" checked> OGEC</td>
                    <td><input type="radio" id="Professeur" name="typeSalarie" value="0"> Professeur</td>
                    <?php
                    }
                    else {
                        
                        ?>
                           <td><input type="radio" id="OGEC" name="typeSalarie" value="1"> OGEC</td>
                            <td><input type="radio" id="Professeur" name="typeSalarie" value="0" checked> Professeur</td>
                             <?php
                    }
                    
                    if($this->unContact){
                        ?>
                    <tr class="ligneTabNonQuad">
                    <td>Contact : </td>
                    <td><input type="checkbox" id="contact" name="contact" value="1" checked></td>
                    </tr>   
                    <?php
                } else {
                ?>
                <tr class="ligneTabNonQuad">
                    <td>Contact : </td>
                    <td><input type="checkbox" id="contact" name="contact" value="1" ></td>
                    </tr>                
                <?php } ?>              
            </table>
            
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <br> <br>
            <a href="index.php">Retour</a>
             
        </form>       
        <?php
        include $this->getPied();
    }

    public function getUnSalarie(): Salarie {
        return $this->unSalarie;
    }

    public function getActionRecue() {
        return $this->actionRecue;
    }

    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }

    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }

    public function setMessage($message) {
        $this->message = $message;
    }
      public function setLesSites($lesSites) {
        $this->lesSites = $lesSites;
    }
    
    public function setUnSite($unSite) {
        $this->unSite = $unSite;
    }
     function setContact(bool $contact){
        $this->unContact = $contact;
    }

}
