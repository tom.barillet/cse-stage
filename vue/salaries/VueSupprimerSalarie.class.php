<?php

namespace vue\salaries;

use vue\VueGenerique;
use modele\metier\Salarie;

/**
 * Description Page de suppression d'un groupe donné
 * @author Tom
 * @version 2020
 */
class VueSupprimerSalarie extends VueGenerique {

    /** @var Groupe groupe à modifier */
    private $unSalarie;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer le salarié
        <?= $this->unSalarie->getPrenom() ?> <?= $this->unSalarie->getNom() ?> ?
            <h3><br>
                <a href="index.php?controleur=salaries&action=validerSupprimer&id=<?= 
                $this->unSalarie->getId() ?>">
                    Oui</a>
                <a href="index.php?controleur=salaries">Non</a></h3></center>
        <?php
        include $this->getPied();
    }
    // ACCESSEURS ET MUTATEURS
    public function getUnSalarie(): Salarie {
        return $this->unSalarie;
    }
    public function getActionRecue() {
        return $this->actionRecue;
    }
    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }
    public function getMessage() {
        return $this->message;
    }
    public function setUnSalarie(Salarie $unSalarie) {
        $this->unSalarie = $unSalarie;
    }
    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }
    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
}